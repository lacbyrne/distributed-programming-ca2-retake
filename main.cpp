//Entry point for the application

#include "SimpleMUD\Player.h"
//#include "LeesEngine\Map.h"
#include "LeesEngine\GameServer.h"
#include "LeesEngine\EventManager.h"
#include "LeesEngine\GameData.h"
#include "LeesEngine\DataStore.h"

#include "SimpleMUD\PlayerDatabase.h"

#include <iostream>

using namespace std;
using namespace LeesEngine;
using namespace ThreadLib;

// **Thread for game updater **/
void gameEventHandler(void* data)
{
	EventManager* timer = (EventManager*)data;
	//timer->setup();

	while(/*timer->isRunning()*/1 )
	{
		if(GameData::m_gameInProg)
		{
		   timer->update(); //...Will probably have to expand to update everything...
		}
	}
}

int main()
{
   //To do : 
   // 1) Load game data
   // 2)set up GameServer here,
   //as well as possibly a thread for the EventManager

   /** Load game data into GameData **/
   DataStore::setupData();
   GameData::setupData();

   //** Set up PlayerDatabase from SimpleMUD engine (while this engine needs to still use that...)
   PlayerDatabase::Load();

   /** Set up server **/
   short ports[] = {9001, 9002, 9003, 9004, 9005};
   GameServer server(ports, 5);
   bool b_gameInProg = true;

   EventManager tm(0);
   ThreadID timeRunner = ThreadLib::Create( gameEventHandler, (void*)&tm);

   srand(unsigned(time(0)));

   while(b_gameInProg)
   {
      //If terminating event occurs --> b_gameInProg = false
	  // ... though a server exit would just kill this anyway, wouldn't it?
	  server.runServer();
   }

   //Don't forget to save stuff when finished!!
   PlayerDatabase::Save();
   GameData::saveAll();

   return 0;
}