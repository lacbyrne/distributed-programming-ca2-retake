#ifndef PLAYER_H
#define PLAYER_H

#include <math.h>
#include <string>
#include "../SocketLib/SocketLib.h"
#include "../BasicLib/BasicLib.h"

#include "Entity.h"
#include "DatabasePointer.h"
#include "../LeesEngine/GameData.h"
//#include "../LeesEngine/ItemIncludes.h"
#include "../LeesEngine/DataStore.h"

//#include "../LeesEngine/Actor.h"
//#include "../LeesEngine/Enemy.h"

#include "../LeesEngine/Typedefs.h"
//#include "../LeesEngine/Communication.h"

using SocketLib::Connection;
using SocketLib::Telnet;
using std::ostream;
using std::istream;
using std::string;

using namespace LeesEngine;
using namespace BasicLib;

namespace SimpleMUD
{
	enum Races
	{
		human,
		troll,
		elf
	};

class Player : public Entity, public Actor
{
    public:

    Player();
	//Constructor for a player that has just been initialised
	Player(string p_name, string p_password, entityid p_id, Attributes p_attributes); 
	//Full constructor
	Player(string p_name, short p_maxHP, short p_currHP, short p_roomIndex, short p_x, short p_y, Attributes p_attributes,
		   string p_race, int p_loot, int p_xp, /*vector<Item> p_inventory,*/ vector<string> p_allies, string p_rank);
	void setActive(bool p_active);
	void move(short p_direction); //Move to relative position
	void moveAbs(short room); //Move to absolute room (esp. the start)
	string getName(); //Not *quite* redundant with Entity.Name() --> overrides Actor.getName(), so thus is used in Actor functions like Attack
	int getAttackTime();
	short getAttackAccuracy();
	short getAttackPower();
	short getEvasion();
	short getDefence();
	short checkKO();
	bool isAlly(string p_name);
	short getDV();
	short getPV();
	short getBlockChance(int p_attackTime);
	/*short*/string acquireItem(Item* item); //Changed from 'getItem' because this is too easy to confuse with accessors
	string setEquipped(string itemName, ItemType type);
	string showInventory();
	string showStats();
	string showAllies();
	void levelUp(); //I think this might have been put in the UML as returning a short by mistake...?
	string offerPeace(Enemy& e);
	void collaborateWith(Player& target);
	void addAlly(/*string name*/Player& ally);
	vector<string> getAllies();
	string giveTo(Item item, Player& receiver);
	string drop(Item item);
	string getPlayerSaveData();
    string getRank();
	string getLastQueryAsked();
	void setLastQueryAsked(string query);
	void setAttributes(Attributes p_attributes);
	short getCharisma() {return m_attributes.charisma;}
	void winBattle(short spoils, bool isXP); //From Actor
	bool isEnemy(); //From Actor
	void respawn();
	bool canAttack();
	Item* getItem(string name, bool remove);
	void useItem(Item* item, Enemy* target);

	//Properties
	short& StatBonuses() {return m_statBonuses;}
	Races& Race() {return m_race;}
	largeNum& TimePassed() {return m_timePassed; }
	short& Level() {return m_level; }
	//int& Loot() {return m_loot;}
	int getLoot() { return m_loot;}
	void setLoot(int loot, bool sharing); //Had to stick in an extra parameter to avoid infinite recursion, since shareLoot uses setLoot
	void shareLoot();
	string& LastQueryAsked() { return m_lastQueryAsked;}
	string& Rank() {  return m_rank; }

    // ------------------------------------------------------------------------
    //  non-savable accessors
    // ------------------------------------------------------------------------
    inline string& Password()               { return m_pass; }
    //inline PlayerRank& Rank()             { return m_rank; }
    inline Connection<Telnet>*& Conn()      { return m_connection; }
    inline bool& LoggedIn()                 { return m_loggedin; }
    inline bool isActive()                   { return m_active; }
    inline bool& Newbie()                   { return m_newbie; }


    // ------------------------------------------------------------------------
    //  Communications Functions
    // ------------------------------------------------------------------------
    void SendString( const string& p_string );
    void PrintStatbar( bool p_update = false );

    // ------------------------------------------------------------------------
    //  Stream Functions
    // ------------------------------------------------------------------------
    friend ostream& operator<<( ostream& p_stream, const Player& p );
    friend istream& operator>>( istream& p_stream, Player& p );

private:
	//**For my own engine**
	Races m_race;
    int m_loot;
	int m_xp;
	short m_level;
	vector<LeesEngine::Item> m_inventory;
	vector<string> m_allies;
	string m_rank;
	string m_lastQueryAsked;
	short m_statBonuses; //Number of points to spend on stat increases earned
	//Need to implement inventory! ={
	//TAKE 1 : Vectors for each item type
	vector<Item* > m_items;
	vector<WieldableItem* > m_wieldableItems;
	vector<Armour* > m_armours;
    vector<MagicItem* > m_magicItems;
	vector<MagicEquipment* > m_magicEquipment;
	largeNum m_timePassed; //Time passed, so far, for the player [should reset when (s)he takes an action?]
	short m_DV; //Storing as an attribute that is updated anything something is equipped or unequipped, rather than recalculated at each attack
	short m_PV; //Ditto

	string statsToString(); //Would this make more sense here?

	//**From SimpleMUD**
    string m_pass; //Password
    //PlayerRank m_rank; //Rank
	//Stuff for handling connection to (human) player
    Connection<Telnet>* m_connection; 
    bool m_loggedin;
    bool m_active;
    bool m_newbie;

};  // end class Player

ostream& operator<<( ostream& p_stream, const Player& p );
istream& operator>>( istream& p_stream, Player& p );

// --------------------------------------------------------------------
//  functor that determines if a player is active
// --------------------------------------------------------------------
struct playeractive
{ 
    inline bool operator() ( Player& p_player )
    {
        return p_player.isActive();
    }

    inline bool operator() ( Player* p_player )
    {
        return p_player != 0 && p_player->isActive();
    }
};

// --------------------------------------------------------------------
//  functor that determines if a player is logged in
// --------------------------------------------------------------------
struct playerloggedin
{ 
    inline bool operator() ( Player& p_player )
    {
        return p_player.LoggedIn();
    }

    inline bool operator() ( Player* p_player )
    {
        return p_player != 0 && p_player->LoggedIn();
    }

};

// --------------------------------------------------------------------
//  functor that sends a string to a player.
// --------------------------------------------------------------------
struct playersend
{
    const string& m_msg;

    playersend( const string& p_msg )
        : m_msg( p_msg ) {  }

    void operator() ( Player& p )
    {
        p.SendString( m_msg );
    }

    void operator() ( Player* p )
    {
        if( p != 0 )
            p->SendString( m_msg );
    }
};

//**Functor added for updating time passed for a(n active) player
struct playerupdate
{
	double m_timePassed;

	playerupdate( double timePassed )
		: m_timePassed(timePassed)
	{

	}

	void operator() ( Player& p)
    {
        p.TimePassed() += m_timePassed;

    }

};

//**Functor added for checking if players are in a given room
struct playerinroom
{
    const short& m_roomIndex;

    playerinroom( short& p_roomIndex )
        : m_roomIndex( p_roomIndex ) {  }

    inline bool operator() ( Player& p )
    {
		return ( p.isActive() && (p.Room() == m_roomIndex) );
    }

};

}  // end namespace SimpleMUD



#endif