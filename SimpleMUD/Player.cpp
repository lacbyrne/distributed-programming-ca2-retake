// MUD Programming
// Ron Penton
// (C)2003
// Player.cpp - The class defining Players in the SimpleMUD
// 
// 

#include <cmath>
#include <string>
#include <fstream>
#include <vector>

#include "SimpleMUDLogs.h"
#include "Player.h"
#include "../LeesEngine/Communication.h"
//#include "../SocketLib/Telnet.h"

using SocketLib::green;
using SocketLib::red;

namespace SimpleMUD
{
	Player::Player() : Actor("", 0, 0, 0, 0, 0), m_allies(0)
    {
       m_pass = "UNDEFINED";
       //m_rank = REGULAR;

       m_connection = 0;
       m_loggedin = false;
       m_active = false;
       m_newbie = true;

	   m_lastQueryAsked = "";
	   m_timePassed = 0;
	   m_DV = 0;
	   m_PV = 0;
}

//Default constructor... or, close to one
	Player::Player(string p_name, string p_password, entityid p_id, Attributes p_attributes)
		: Actor(p_name, 0, 0, 0, 0, 0), m_items(), m_wieldableItems(), m_armours(), m_magicItems(), m_magicEquipment(), m_allies(0)
	{
		m_name = p_name; //Need to pass it in here (too)! -- Actor doesn't actually do anything with it

		m_pass = "UNDEFINED";
        //m_rank = REGULAR;

        m_connection = 0;
        m_loggedin = false;
        m_active = false;
        m_newbie = true;

		m_pass = p_password;
		m_id = p_id;

		m_attributes = p_attributes;
		//m_race = ""; //Need to try and parse string input into an Enum somehow
		m_race = Races::human;
		m_loot = 0;
		m_xp = 0;
		m_level = 1;
		m_rank = "noob";
		m_lastQueryAsked = "";
		m_statBonuses = 5;

		m_timePassed = 0;
		m_DV = 0;
		m_PV = 0;
	}

	Player::Player(string p_name, short p_maxHP, short p_currHP, short p_roomIndex, short p_x, short p_y, Attributes p_attributes,
			      string p_race, int p_loot, int p_xp, /*vector<Item> p_inventory,*/ vector<string> p_allies, string p_rank)
				  : Actor(p_name, p_maxHP, p_currHP,/*p_attributes,*/ p_roomIndex, p_x, p_y)
	{
		m_lastQueryAsked = "";
		m_timePassed = 0;
		m_DV = 0;
		m_PV = 0;
	}

	string Player::getName()
	{
		return m_name;
	}

	void Player::setActive(bool p_active)
	{
		if(p_active)
		{
		   m_active = true;
		   GameData::m_gameInProg = true;
		}
		else
		{
			m_active = false;
			if( !Communication::findAnActivePlayer())
			{
				GameData::m_gameInProg = false;
			}
		}
	}

	short Player::getAttackAccuracy()
	{
		short weapAcc;
		//Get accuracy of equipped weapon, if any
		size_t i = 0;
		for(i = 0; i < m_wieldableItems.size(); i++)
	    {
			if(m_wieldableItems[i]->isEquipped() )
			{
				if(m_wieldableItems[i]->isTwoHanded())
				{
					return (m_attributes.strength/2) + m_wieldableItems[i]->getAccuracy();
				}
				else
				{
					return (m_attributes.dexterity) + m_wieldableItems[i]->getAccuracy();
				}
			}
	    }

		//No weapon... =|
		return m_attributes.dexterity;
	}

	short Player::getEvasion()
	{
		return m_attributes.agility + m_DV;
	}

	short Player::getAttackPower()
	{
		short power = m_attributes.strength;
		size_t i = 0;
		for(i = 0; i < m_wieldableItems.size(); i++)
	    {
			if(m_wieldableItems[i]->isEquipped() )
			{
				power += m_wieldableItems[i]->getPower();
			}
	    }

		return power;
	}

	short Player::getDefence()
	{
		return m_PV + m_attributes.endurance/3;
	}

	short Player::checkKO()
	{
		if(m_currHP < 1)
		{
		   Communication::sendRoom(m_name + " has fallen!", m_roomIndex );
		   respawn();
		   //Don't forget to BROADCAST when a player falls, too!
		   m_loot /= 2;
		   //return m_loot;
		   return m_loot; //test
		}
		
		return 0;
	}

    //Prototypical move =P
	void Player::move(short direction)
	{
		short destination = 0;

        switch(direction)
		{
		   case 1: //north
		   destination = GameData::m_map.getRoom(m_roomIndex).getExits().north;
		   break;

		   case 2: //south
		   destination = GameData::m_map.getRoom(m_roomIndex).getExits().south;
		   break;

		   case 3: //west
		   destination = GameData::m_map.getRoom(m_roomIndex).getExits().west;
		   break;

		   case 4: //east
		   destination = GameData::m_map.getRoom(m_roomIndex).getExits().east;
		   break;
		}

		if(destination == -1)
		{
			//Can't go that way!
		}
		else
		{
			GameData::m_map.getRoom(m_roomIndex).removePlayer(m_name);
			//Communication::sendRoom(m_name + " leaves.", m_roomIndex);
			m_roomIndex = destination;
			GameData::m_map.getRoom(m_roomIndex).addPlayer(m_name);
			//Communication::sendRoom(m_name + " enters!", m_roomIndex);
		}
	}

	void Player::moveAbs(short room)
	{
		GameData::m_map.getRoom(m_roomIndex).removePlayer(m_name);
		//Communication::sendRoom(m_name + " leaves.", m_roomIndex);
		m_roomIndex = room;
		GameData::m_map.getRoom(m_roomIndex).addPlayer(m_name);
		//Communication::sendRoom(m_name + " enters!", m_roomIndex);
	}

	int Player::getAttackTime()
	{
	   int attackTime = 3000 / ( (m_attributes.dexterity/5)+5) ;
		//Get equipped weapon
		size_t i;
		for(i = 0; i < m_wieldableItems.size(); i++)
	    {
			if(m_wieldableItems[i]->isEquipped() )
		    {
				attackTime *= m_wieldableItems[i]->getSwingTime();
				break;
			}
	    }
		for(i = 0; i < m_armours.size(); i++)
	    {
			if(m_wieldableItems[i]->isEquipped() )
		    {
				attackTime *= m_wieldableItems[i]->getSwingTime();
				break;
			}
	    }

		return attackTime;
	}
 
	bool Player::isAlly(string p_name)
	{
		vector<string>::iterator itr = m_allies.begin();
			
		for(itr = m_allies.begin(); itr != m_allies.end(); ++itr)
		{
		   if(*itr == p_name)
		   {
		      return true;  
		   }
		}

		return false;
	}

	void Player::setAttributes(Attributes p_attributes)
	{
		m_attributes = p_attributes;
	}

	//Give that player a... something!
	string Player::acquireItem(Item* item)
	{
		if(item == 0)
		{
			return "Couldn't find one of those...";
		}

		WieldableItem* wi = (WieldableItem*)item;
		Armour* arm = (Armour*)item;
		MagicItem* mi = (MagicItem*)item;
		MagicEquipment* me = (MagicEquipment*)item;

		switch(item->getType() )
		{
		   case ItemType::BasicItemType:
		   m_items.push_back( new Item(*item) );
		   break;

		   case ItemType::WieldableItemType:
		   m_wieldableItems.push_back( new WieldableItem(*wi) );
		   break;

		   case ItemType::ArmourType:
		   m_armours.push_back( new Armour(*arm) );
		   break;

		   case ItemType::MagicItemType:
		   m_magicItems.push_back( new MagicItem(*mi) );
		   break;

		   case ItemType::MagicEquipmentType:
		   m_magicEquipment.push_back( new MagicEquipment(*me) );
		   break;
		}

		return "Got a(n) " + item->Name();
	}

	string Player::showInventory()
	{
        string toReturn = "=======================================\r\nWIELDABLE ITEMS:\r\n";
		size_t i = 0; //size_t is variable type using in looping through a vector [prevents compile warnings]
		for(i = 0; i < m_wieldableItems.size(); i++)
	    {
		    toReturn += m_wieldableItems[i]->Name();
			if(m_wieldableItems[i]->isEquipped() )
			{
				toReturn += " (equipped)";
			}
			toReturn += "\r\n";
	    }

		toReturn += "ARMOURS:\r\n";
	    for(i = 0; i < m_armours.size(); i++)
	    {
		    toReturn += m_armours[i]->Name();
			if(m_armours[i]->isEquipped() )
			{
				toReturn += " (equipped)";
			}
			toReturn += "\r\n";
	    }

		toReturn += "MAGIC ITEMS:\r\n";
	    for(i = 0; i < m_magicItems.size(); i++)
	    {
			toReturn += m_magicItems[i]->Name() + "\r\n"; 
	    }
		 
		toReturn += "ENCHANTED EQUIPMENT:\r\n";
	    for(i = 0; i < m_magicEquipment.size(); i++)
	    {
			toReturn += m_magicEquipment[i]->Name();
			if(m_magicEquipment[i]->isEquipped() )
			{
				toReturn += " (equipped)";
			}
			toReturn += "\r\n";
		}

		toReturn += "MISC ITEMS:\r\n";
		for(i = 0; i < m_items.size(); i++)
	    {
			toReturn += m_items[i]->Name() + "\r\n"; 
	    }

		return toReturn;
	}

	string Player::setEquipped(string itemName, ItemType type)
	{
		size_t i = 0;

		if(type == ItemType::WieldableItemType || type == ItemType::BasicItemType)
		{
		      for(i = 0; i < m_wieldableItems.size(); i++)
	          {
				  if(m_wieldableItems[i]->Name().find(itemName) != -1)
				  {
					  if(m_wieldableItems[i]->isEquipped() == false)
					  {
					     m_wieldableItems[i]->isEquipped() = true;
					     //Update DV/PV
						 m_DV += m_wieldableItems[i]->getDV();
						 m_PV += m_wieldableItems[i]->getPV();

						 //Unequip what is previously equipped
						 short indexEquipped = i;

						 for(i = 0; i < m_wieldableItems.size(); i++)
						 {
							 if(i != indexEquipped)
							 {
								 if(m_wieldableItems[i]->isEquipped() )
								 {
								    //Factor out its modifiers before unequipping
									m_DV -= m_wieldableItems[i]->getDV();
						            m_PV -= m_wieldableItems[i]->getPV();
									m_wieldableItems[i]->isEquipped() = false;
								 }
							 }
						 }
						 return m_wieldableItems[indexEquipped]->Name() + " is now being wielded";

					  }
					  else
					  {
						  return "You're already wielding that!";
					  }
				  }
	          }
		}


	    if(type == ItemType::ArmourType || type == ItemType::BasicItemType)
	    {
		   for(i = 0; i < m_armours.size(); i++)
	       {
		      if(m_armours[i]->Name().find(itemName) != -1)
			  {
			     if(m_armours[i]->isEquipped() == false)
				 {
				    m_armours[i]->isEquipped() = true;
					//Update DV/PV
					m_DV += m_armours[i]->getDV();
					m_PV += m_armours[i]->getPV();

					//Unequip what is previously equipped
					short indexEquipped = i;

					for(i = 0; i < m_armours.size(); i++)
					{
					   if(i != indexEquipped)
					   {
					      if(m_armours[i]->isEquipped() )
						  {
						     //Factor out its modifiers before unequipping
							 m_DV -= m_armours[i]->getDV();
						     m_PV -= m_armours[i]->getPV();
						     m_armours[i]->isEquipped() = false;
						  }
					   }
					}

					return m_armours[indexEquipped]->Name() + " is now being worn";
			     }
				 else
				 {
				    return red + "You're already wearing that!";
				 }
		      }
	       }

	       for(i = 0; i < m_magicEquipment.size(); i++)
	       {
		      if(m_magicEquipment[i]->Name().find(itemName) != -1)
			  {
			     if(m_magicEquipment[i]->isEquipped() == false)
				 {
				    m_magicEquipment[i]->isEquipped() = true;
					//Will need to make sure this updates any stats!
				    //...Just check DV for the mo'
				    //...Actually, deal with this later =P

					//Unequip what is previously equipped
					short indexEquipped = i;

					for(i = 0; i < m_magicEquipment.size(); i++)
					{
					   if(i != indexEquipped)
					   {
					      if(m_magicEquipment[i]->isEquipped() )
						  {
						     //Factor out its modifiers before unequipping
							 m_magicEquipment[i]->isEquipped() = false;
						  }
					   }
					}
					return m_magicEquipment[indexEquipped]->Name() + " is now being worn";
			     }
				 else
				 {
				    return red + "You're already wearing that!";
				 }
		      }
	       }
		}

		return "...What?";
	}

	string Player::showStats()
	{
		string toReturn = " ====================STATS==================== \r\n Level : " + BasicLib::tostring(m_level) + "\r\n" +
		   " XP : " + BasicLib::tostring(m_xp) + "\t\t" +
		   " Loot: " + BasicLib::tostring(m_loot) + "\r\n" +
		   " HP: " + BasicLib::tostring(m_currHP) + " / " + BasicLib::tostring(m_maxHP) + "\r\n" + 
		   " Strength: " + BasicLib::tostring(m_attributes.strength) + "\t\t" +
		   " Intelligence: " + BasicLib::tostring(m_attributes.intelligence) + "\r\n" +
		   " Endurance: " + BasicLib::tostring(m_attributes.endurance) + "\t\t" +
		   " Spirit: " + BasicLib::tostring(m_attributes.spirit) + "\r\n" +
		   " Dexterity: " + BasicLib::tostring(m_attributes.dexterity) + "\t\t" +
		   " Charisma: " + BasicLib::tostring(m_attributes.charisma) + "\r\n" +
		   " Agility: " + BasicLib::tostring(m_attributes.agility) + "\r\n==============================================";

		return toReturn;
	}

	string Player::showAllies()
	{
		if(m_allies.size() == 0)
		{
		   return "...You're a lone wolf.";
		}
		else
		{
			string toReturn = yellow + " ==========YOUR TEAM==========\r\n";
			vector<string>::iterator itr = m_allies.begin();
			
			for(itr = m_allies.begin(); itr != m_allies.end(); ++itr)
			{
			   if(*itr != "")
			   {
			      toReturn += *itr + "\r\n";   
			   }
			}

			return toReturn;
		}

		return "";
	}

	void Player::winBattle(short spoils, bool isXP)
	{
		if(isXP)
		{
			m_xp += spoils;
			SendString(green + "You earned " + BasicLib::tostring(spoils) + " xp");
			//Check level up
			levelUp();
		}
		else
		{
			SendString("The target surrenders, giving over " + BasicLib::tostring(spoils) + " loot!");
			setLoot( m_loot + spoils, true);
		}
	}

	void Player::levelUp()
	{
		//For now...
		if(m_xp >= (m_level * 20) + (m_level * 15) )
		{
			m_level += 1;
			m_statBonuses += 2; //Restrict this to somewhat balance the game.. =P
			
			if(m_rank == "noob" && m_level >= 5)
			{
			   SendString(green + " You were promoted from a Noob to a Regular user");
			   m_rank = "regular";
			}
		}
	}

	bool Player::isEnemy()
	{
		return false;
	}

	void Player::respawn()
	{
		moveAbs(0);
		SendString("...You wake up back at the start of the dungeon, with half your loot mysteriously missing.");
		SendString( GameData::m_map.getRoom(m_roomIndex).getDescription() + "\r\n"
			        + GameData::m_map.showRoomExits(m_roomIndex )
					+ "\r\nInhabitants: \r\n" + GameData::m_map.getRoom( m_roomIndex ).showInhabitants() );
		m_currHP = m_maxHP;
	}

	bool Player::canAttack()
	{
		
		if(m_timePassed > getAttackTime() )
		{
			m_timePassed = 0;
			return true;
		}

		return false;
	}

	Item* Player::getItem(string name, bool remove)
	{
	   Item* toReturn = 0;

	   //Have to search entire inventories for a specific item... =|
	   size_t i = 0; //size_t is variable type using in looping through a vector [prevents compile warnings]
	   for(i = 0; i < m_wieldableItems.size(); i++)
	    {
			if(m_wieldableItems[i]->Name() == name)
			{
				toReturn = m_wieldableItems[i];
				if(remove)
				{
					m_wieldableItems.erase( m_wieldableItems.begin() + i ); //Note that erase works on *positions*
				}
				return toReturn;
			}
	    }

	    for(i = 0; i < m_armours.size(); i++)
	    {
			if(m_armours[i]->Name() == name )
			{
				toReturn = m_armours[i];
				if(remove)
				{
					m_armours.erase( m_armours.begin() + i );
				}
				return toReturn;
			}
	    }

	    for(i = 0; i < m_magicItems.size(); i++)
	    {
			if(m_magicItems[i]->Name() == name )
			{
				toReturn = m_magicItems[i];
				if(remove)
				{
					m_magicItems.erase( m_magicItems.begin() + i );
				}
				return toReturn;
			}
	    }
		 
	    for(i = 0; i < m_magicEquipment.size(); i++)
	    {
			if(m_magicEquipment[i]->Name() == name)
			{
				toReturn = m_magicEquipment[i];
				if(remove)
				{
					m_magicEquipment.erase( m_magicEquipment.begin() + i );
				}
				return toReturn;
			}
		}

		for(i = 0; i < m_items.size(); i++)
	    {
			if(m_items[i]->Name() == name)
			{
				toReturn = m_items[i];
				if(remove)
				{
					m_items.erase( m_items.begin() + i );
				}
				return toReturn;
			}
		}

		return 0;
	}

	void Player::setLoot(int loot, bool sharing)
    {
       m_loot = loot;

	  //Collected loot should be shared amongst the team...?
	   if(sharing)
	   {
	      if(m_allies.size() > 0)
	      {
		     shareLoot();
	      }
	   }

	   //Check if player has topped the scoreboard
       PlayerDatabase::iterator itr;
       for(itr = PlayerDatabase::begin(); itr != PlayerDatabase::end(); ++itr)
       {
         if(itr->getLoot() > m_loot)
	     {
  	        return;
	     }
      }

      //Else, player has highest loot
      SendString(green + "Huzzah, you have the highest loot out of all current explorers! ^_^");
	}

	//If working with at least one player, the loot should be shared around the team
	void Player::shareLoot()
	{
	   //...
		int totalLoot = m_loot;
		vector<string>::iterator itr = m_allies.begin();
		for(itr = m_allies.begin(); itr != m_allies.end(); ++itr)
		{
			//Loading an empty allies string from a text file seems to introduce garbage =|
			if(*itr != "")
			{
			   Player& p = *PlayerDatabase::findfull(*itr);
			   totalLoot += p.getLoot();
			}
		}

		int sharedLoot = totalLoot / (m_allies.size()+1);

		for(itr = m_allies.begin(); itr != m_allies.end(); ++itr)
		{
			if(*itr != "")
			{
			   Player& p = *PlayerDatabase::findfull(*itr);
			   p.setLoot( sharedLoot, false ); 
			   p.SendString(yellow + "Team loot: " + BasicLib::tostring(sharedLoot) );
			}
		}

		//Oh yeah... and the player's own loot =|
		m_loot = sharedLoot;
	}

	void Player::collaborateWith(Player& target)
	{
	    //Oh! Need to check if not already teaming up with the target
		vector<string>::iterator itr = m_allies.begin();
		for(itr = m_allies.begin(); itr != m_allies.end(); ++itr)
		{
		   if(*itr == target.Name() )
		   {
			   SendString( target.Name() + " already agreed to team up with you, remember? =) ");
			   return;
		   }
		}

  	    //For fun... have the 'motivating' message depend on the propagator's charisma =)
		string message = m_name;
		if(getCharisma() < 5)
		{
		   message += " seems to want to work with you. Will you collaborate with " + m_name + "?";
		}
		else if(getCharisma() < 10)
		{
		   message += " really wants to team up with you. Will you collaborate with " + m_name + "?";
		}
		else //Reeallly persuasive!
		{
		   message += " persuades you that working with him/her with be of great mutual benefit. \r\nWill you pleeeease collaborate with " 
			       + m_name + "? =] ";
		}

		target.LastQueryAsked() = message;
		target.SendString(green + target.LastQueryAsked());
	}

	void Player::addAlly(/*string name*/Player& ally)
	{
		m_allies.push_back(ally.Name() );

		//...almost forgot that all allies of the player will also become part of the player's team =|
		vector<string> allyAllies = ally.getAllies();
		vector<string>::iterator allItr = m_allies.begin();
		vector<string>::iterator aaItr = allyAllies.begin();

		bool newAlly = true;

		for(aaItr = allyAllies.begin(); aaItr != allyAllies.end(); ++aaItr)
		{
		   newAlly = true;

		   if(*aaItr == m_name)
		   {
		      newAlly = false;
		   }
		   else
		   {
		      for(allItr = m_allies.begin(); allItr != m_allies.end(); ++allItr)
		      {
		         if(*aaItr == *allItr)
				 {
				    newAlly = false;
					break;
				 }
		      }   
		   }

		   if(newAlly)
		   {
			   m_allies.push_back(*aaItr);
			   //And now this player is the new ally's ally! [god, using these sort of terms is bound to get confusing...]
			   PlayerDatabase::findfull( *aaItr)->addAlly(*this); 
		   }
		}

		//Share loot...
		shareLoot();

		SendString( showAllies() ); //In case the player starts to lose track of their new team (from allies' allies and allies' allies' allies)

		PlayerDatabase::Save();
	}

	vector<string> Player::getAllies()
	{
	   return m_allies;
	}

	void Player::useItem(Item* item, Enemy* target)
	{
		if(item->getType() != ItemType::MagicItemType)
		{
			if(item->getType() == ItemType::WieldableItemType)
			{
			   SendString("You must either *wield* or *equip* this, and use it in combat");
		    }
			else if(item->getType() == ItemType::ArmourType)
			{
				SendString("You must either *wear* or *equip* this. It protects you from attacks");
			}
			else if(item->getType() == ItemType::MagicEquipmentType)
			{
				SendString("You must either *wear* or *equip* this. It has some magical effects");
			}
			else
			{
				SendString( item->getUse() );
			}
		}
		else
		{
            MagicItem* mi = (MagicItem*)item;
			if(mi->getEffect() < 0) //Heals
			{
			   //Just own? Or expand to allies?
				short effect = (mi->getEffect())*-1; //*Might* want to expand this to include random elements
				if(m_currHP + effect > m_maxHP)
				{
					m_currHP = m_maxHP;
				}
				else
				{
					m_currHP += effect;
				}

				SendString("You recover " + BasicLib::tostring(effect) + " HP");
				mi->Charges() -= 1;
			}
			else
			{
			   if(target == 0)
			   {
				   m_lastQueryAsked = "You activate your " + mi->Name() + ". Select a target :";
				   SendString(yellow + m_lastQueryAsked);
				   SendString("Enemies: " + GameData::m_map.getRoom( m_roomIndex).listEnemies() + reset);
			   }
			   else
			   {
				   SendString("You unleash your magic on the " + target->getName() + "!");
				   short power = mi->getEffect() + (m_attributes.intelligence*1.5);
				   short accuracy = m_attributes.intelligence + m_attributes.dexterity;
				   SendString(specialAttack(*target, accuracy, power));
				   
				   mi->Charges() -= 1;
			   }
			}

			//If all charges are spent, get rid of the item!
			if(mi->Charges() < 1)
			{
				vector<MagicItem*>::iterator itr = m_magicItems.begin();
				for(itr = m_magicItems.begin(); itr != m_magicItems.end(); ++itr)
				{
					if( (*itr) == mi)
					{
						SendString( red + mi->Name() + " is used up. It suddenly disappears!");
						m_magicItems.erase( itr );
						return;
					}
				}
			}
		}
	}

// ------------------------------------------------------------------------
//  This sends a string to the players connection.
// ------------------------------------------------------------------------
void Player::SendString( const std::string& p_string )
{
    using namespace SocketLib;

    if( Conn() == 0 )
    {
        ERRORLOG.Log( "Trying to send string to player " +
                      Name() + " but player is not connected." );
        return;
    }

    // send the string, "\r\n" included.
    Conn()->Protocol().SendString( *Conn(), p_string + "\r\n");

	if( m_active )
    {
        PrintStatbar();
    }
}

// ------------------------------------------------------------------------
//  This prints up the players "statbar"
// ------------------------------------------------------------------------
void Player::PrintStatbar( bool p_update )
{
    using namespace SocketLib;
    using namespace BasicLib;

    // if this is a statusbar update and the user is currently typing something,
    // then do nothing.
    if( p_update && Conn()->Protocol().Buffered() > 0 )
        return;

    string statbar = white + bold + "[";

	int ratio = 100 * m_currHP / m_maxHP;

    // color code your hitpoints so that they are red if low,
    // yellow if medium, and green if high.
    if( ratio < 33 )
        statbar += red;
    else if( ratio < 67 )
        statbar += yellow;
    else 
        statbar += green;

    statbar += tostring( m_currHP ) + white + "/" + 
		tostring( m_maxHP ) + "] ";

	string buffered = Conn()->Protocol().CurrentInput();

    Conn()->Protocol().SendString( *Conn(), "\r" + statbar + buffered + reset );
}

// --------------------------------------------------------------------
//  Inserts an item in text form into a stream
// --------------------------------------------------------------------
ostream& operator<<( ostream& p_stream, const Player& p )
{
    p_stream << "<NAME>" << p.m_name << "\n";
    p_stream << "<PASS>" << p.m_pass << "\n";
	p_stream << "<MAX HP>" << p.m_maxHP << "\n";
	p_stream << "<CURR HP>" << p.m_currHP << "\n";
	p_stream << "<STRENGTH>" << p.m_attributes.strength << "\n";
	p_stream << "<ENDURANCE>" << p.m_attributes.endurance << "\n";
	p_stream << "<DEXTERITY>" << p.m_attributes.dexterity << "\n";
	p_stream << "<AGILITY>" << p.m_attributes.agility << "\n";
	p_stream << "<INTELLIGENCE>" << p.m_attributes.intelligence << "\n";
	p_stream << "<SPIRIT>" << p.m_attributes.spirit << "\n";
	p_stream << "<CHARISMA>" << p.m_attributes.charisma << "\n";
	p_stream << "<ROOM INDEX>" << p.m_roomIndex << "\n";
	p_stream << "<X>" << p.m_x << "\n";
	p_stream << "<Y>" << p.m_y << "\n";
    p_stream << "<RACE>" << p.m_race << "\n";
    p_stream << "<LOOT>" << p.m_loot << "\n";
	p_stream << "<XP>" << p.m_xp << "\n";
	p_stream << "<LEVEL>" << p.m_level << "\n";

	//Save inventory
	p_stream << "<INVENTORY>";
	string items = "";
	//Iterate through the vectors the 'lazy way', at least until it is established that this works
	size_t i = 0;
	for(i = 0; i < p.m_wieldableItems.size(); i++)
	{
		if(items != "")
		{
           items += ", ";
		}
		items += p.m_wieldableItems[i]->Name();
	}
	for(i = 0; i < p.m_armours.size(); i++)
	{
		if(items != "")
		{
           items += ", ";
		}
		items += p.m_armours[i]->Name();
	}
	for(i = 0; i < p.m_magicItems.size(); i++)
	{
		if(items != "")
		{
           items += ", ";
		}
		items += p.m_magicItems[i]->Name();
	}
	for(i = 0; i < p.m_magicEquipment.size(); i++)
	{
		if(items != "")
		{
           items += ", ";
		}
		items += p.m_magicEquipment[i]->Name();
	}
	for(i = 0; i < p.m_items.size(); i++)
	{
		if(items != "")
		{
           items += ", ";
		}
		items += p.m_items[i]->Name();
	}
	p_stream << items << "\n";

	//Save allies...?]
	p_stream << "<ALLIES>";
	//vector<string>::const_iterator alliesItr;
	//for(alliesItr = p.m_allies.begin(); alliesItr != p.m_allies.end(); ++alliesItr)
	for(i = 0; i < p.m_allies.size(); i++)
	{
		//p_stream << *alliesItr;
		p_stream << p.m_allies[i];

		if(i + 1 != p.m_allies.size() )
		{
		   p_stream << ",";
	    }
	}
	p_stream << "\n";
	p_stream << "<RANK>" << p.m_rank << "\n";
	p_stream << "<STAT BONUSES> " << p.m_statBonuses; //Put space at end or not?

    return p_stream;
}


// --------------------------------------------------------------------
//  Extracts an item in text form from a stream
// --------------------------------------------------------------------
istream& operator>>( istream& p_stream, Player& p )
{
	// **Lee's much less elegant approach to this... since the way Penton did it kinda went over my head =| 
	vector<string> collectedData;
	string data;

	while(p_stream.good() )
	{
		getline(p_stream, data);
		//Name
		collectedData.push_back(data.substr(data.find('>')+1, data.length()));
	}

	vector<string>::iterator iter = collectedData.begin() + 1;

	p.m_name = *iter;
	++iter;
	p.m_pass = *iter;
	++iter;
	p.m_maxHP = atoi(iter->c_str());
	++iter;
	 p.m_currHP = atoi(iter->c_str());;
	++iter;
	p.m_attributes.strength = atoi(iter->c_str());
	++iter;
	p.m_attributes.endurance = atoi(iter->c_str());
	++iter;
	p.m_attributes.dexterity = atoi(iter->c_str());
	++iter;
	p.m_attributes.agility = atoi(iter->c_str());
	++iter;
	p.m_attributes.intelligence = atoi(iter->c_str());
	++iter;
	p.m_attributes.spirit = atoi(iter->c_str());
	++iter;
	p.m_attributes.charisma = atoi(iter->c_str());
	++iter;
	p.m_roomIndex = atoi(iter->c_str());
	++iter;
	p.m_x = atoi(iter->c_str());
	++iter;
	p.m_y = atoi(iter->c_str());
	++iter;
	//Parse race
	short race = atoi(iter->c_str());
	switch(race)
	{
	    case Races::human:
		p.m_race = Races::human;
		break;

		case Races::troll:
		p.m_race = Races::troll;
		break;

		case Races::elf:
		p.m_race = Races::elf;
		break;
	}

	++iter;
    p.m_loot = atoi(iter->c_str());
	++iter;
	p.m_xp = atoi(iter->c_str());
	++iter;
    p.m_level = atoi(iter->c_str());
	++iter;

	//Now time to load up the inventory!
	//Strategy to run with for now: store inventory as a string of items and use GameData::m_itemTypes to translate name to attributes
	vector<string> storedItems;
	string items = *iter;
	//string item;
	while(items.find(',') != -1)
	{
		storedItems.push_back(items.substr(0, items.find(',') ) );
		//modifiers.push_back( item );
		items = items.substr(items.find(',')+2, items.length());
	}
	//Above logic leaves the last item in the string remaining to be found
	if(items != "")
	{
	   storedItems.push_back( items );
	}

	vector<string>::iterator itemsItr = storedItems.begin();
	for(itemsItr = storedItems.begin(); itemsItr != storedItems.end(); ++itemsItr)
	{
		Item* item = DataStore::getSpecificItem( *itemsItr );
		
		//Need to stick in the inventory now, somewhere...
		p.acquireItem(item);
	}

	++iter;
	//Parse allies... somehow
	string allies = *iter;
	bool allParsed = false;
	string ally;

	while( !allParsed)
	{
		if( allies.find(',') == -1)
		{
			allParsed = true;
			//...Actually means that the last ally is about to be parsed, whopps =}
			if(allies != "")
			{
			   ally = allies;
			   p.m_allies.push_back(ally);
			}
		}
		else
		{
			ally = allies.substr(0, allies.find(','));
			p.m_allies.push_back(ally);
			allies = allies.substr(allies.find(',')+1, allies.length());
		}

		cout << ally;
	}

	++iter;
	p.m_rank = *iter;
	++iter;
	p.m_statBonuses = atoi(iter->c_str());

    return p_stream;
}

}   // end namespace 