// MUD Programming
// Ron Penton
// (C)2003
// Game.cpp - This class is the game handler for SimpleMUD.
// 
// 
#include "Logon.h"
#include "Game.h"
#include "../LeesEngine/LoggedIn.h"
#include "../LeesEngine/CharacterSetup.h"
#include "PlayerDatabase.h"
#include "../BasicLib/BasicLib.h"
#include "../LeesEngine/DataStore.h"

using namespace SocketLib;
using namespace BasicLib;
using std::string;

using namespace LeesEngine;

namespace SimpleMUD
{

// ------------------------------------------------------------------------
//  This handles incomming commands. Anything passed into this function
//  is assumed to be a complete command from a client.
// ------------------------------------------------------------------------
void Game::Handle( string p_data )
{
    Player& p = *m_player;

    //check if the player wants to repeat a command
    if( p_data == "/" )
    {
        p_data = m_lastcommand;
    }
    else
    {
        // if not, record the command.
        m_lastcommand = p_data;
    }

	short playerLevel = m_player->Level();
	string feedback = Parser::executePlayerInput( BasicLib::LowerCase(p_data), p);

	//Update player state
	if(m_player->Level() != playerLevel)
	{
	   //Assume level up?
	   m_player->SendString(green + "YUSS!! Levelled up!! <=D ");
	   m_player->Conn()->AddHandler(new CharacterSetup(*m_player->Conn(), m_player));
	}

	if(feedback == "exit")
	{
	   m_player->Conn()->RemoveHandler();
	}
}


// ------------------------------------------------------------------------
//  This notifies the handler that there is a new connection
// ------------------------------------------------------------------------
void Game::Enter()
{
    USERLOG.Log(  GetIPString( m_connection->GetRemoteAddress() ) + 
                  " - User " + m_player->Name() + 
                  " entering Game state." );

    m_lastcommand = "";

    Player& p = *m_player;

	p.setActive(true);

	 SendGame( bold + green + p.Name() + " gets back in the game!" );

	 //Spawn in room
	 GameData::m_map.getRoom( p.Room() ).addPlayer( p.Name() );
}

void Game::Leave()
{
    // deactivate player
	SendGame(m_player->Name() + " left all of a sudden...");
    m_player->setActive(false);

	//...?
	PlayerDatabase::SavePlayer(m_player);

	GameData::m_map.getRoom( m_player->Room() ).removePlayer( m_player->Name() );

    // log out the player from the database if the connection has been closed
    if( m_connection->Closed() )
    {
        PlayerDatabase::Logout( m_player );
    }
}

// ------------------------------------------------------------------------
//  This notifies the handler that a connection has unexpectedly hung up.
// ------------------------------------------------------------------------
void Game::Hungup()
{
    Player& p = *m_player;
    LogoutMessage( p.Name() + " has suddenly disappeared from the realm." );
}

// ------------------------------------------------------------------------
//  This notifies the handler that a connection is being kicked due to 
//  flooding the server.
// ------------------------------------------------------------------------
void Game::Flooded()
{
    Player& p = *m_player;
    LogoutMessage( p.Name() + " has been kicked for flooding!" );
}

// ------------------------------------------------------------------------
//  Sends a string to everyone connected.
// ------------------------------------------------------------------------
void Game::SendGlobal( const string& p_str )
{
    operate_on_if( PlayerDatabase::begin(),
                   PlayerDatabase::end(),
                   playersend( p_str ),
                   playerloggedin() );
}

// ------------------------------------------------------------------------
//  Sends a string to everyone "within the game"
// ------------------------------------------------------------------------
void Game::SendGame( const std::string& p_str )
{
    operate_on_if( PlayerDatabase::begin(),
                   PlayerDatabase::end(),
                   playersend( p_str ),
                   playeractive() );
}

// ------------------------------------------------------------------------
//  Sends a logout message
// ------------------------------------------------------------------------
void Game::LogoutMessage( const string& p_reason )
{
    SendGame( SocketLib::red + SocketLib::bold + p_reason );
}

// ------------------------------------------------------------------------
//  Sends a system announcement
// ------------------------------------------------------------------------
void Game::Announce( const string& p_announcement )
{
    SendGlobal( SocketLib::cyan + SocketLib::bold + 
                "System Announcement: " + p_announcement );
}

}   // end namespace