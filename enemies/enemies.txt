{
Name: Kobold
MaxHP: 5
Strength: 3
Endurance: 2
Dexterity: 7
Agility: 9
Intelligence: 2
Spirit: 2
XPYield: 5
Aggression: 5
SpecName: throws a rock at
SpecPower: 4
SpecAcc: 6
SpecChance: 4
SpecRecharge: 3000
SpecMagic: n
Description: These nasty little vermin are very common in the dungeon. They are hardly a threat at all.
DangerLevel: 1
}
{
Name: Goblin
MaxHP: 8
Strength: 3
Endurance: 2
Dexterity: 8
Agility: 8
Intelligence: 4
Spirit: 2
XPYield: 10
Aggression: 5
SpecName: throws a rock at
SpecPower: 4
SpecAcc: 6
SpecChance: 4
SpecRecharge: 3000
SpecMagic: n
Description: A common little creature that is not terribly strong or tough
DangerLevel: 1
}
{
Name: Skeleton
MaxHP: 5
Strength: 4
Endurance: 2
Dexterity: 9
Agility: 7
Intelligence: 0
Spirit: 6
XPYield: 5
Aggression: 7
SpecName: throws a bone at
SpecPower: 7
SpecAcc: 8
SpecChance: 4
SpecRecharge: 5000
SpecMagic: n
Description: Some pesky necromancer animated the skeleton of a poor fallen foe. Their current form is brittle, but quick and nimble.
DangerLevel: 1
}
{
Name: Zombie
MaxHP: 20
Strength: 5
Endurance: 4
Dexterity: 2
Agility: 2
Intelligence: 0
Spirit: 5
XPYield: 12
Aggression: 3
SpecName: does a head smash at
SpecPower: 10
SpecAcc: 4
SpecChance: 2
SpecRecharge: 10000
SpecMagic: n
Description: Yuck! Some vile necromancer has animated the corpse of a fallen adventurer. This lifeform is numb and absent-minded, and you would feel sorry for it if it didn't smell so bad.
DangerLevel: 2
}
{
Name: Orc
MaxHP: 20
Strength: 6
Endurance: 6
Dexterity: 3
Agility: 3
Intelligence: 3
Spirit: 1
XPYield: 15
Aggression: 8
SpecName: charges at
SpecPower: 10
SpecAcc: 2
SpecChance: 3
SpecRecharge: 7000
SpecMagic: n
Description: A nasty, pig-faced brute that likes fighting. They are not particularly smart, but are good at following orders.
DangerLevel: 2
}
{
Name: Ogre
MaxHP: 30
Strength: 8
Endurance: 7
Dexterity: 3
Agility: 1
Intelligence: 2
Spirit: 1
XPYield: 20
Aggression: 8
SpecName: does a body slam on
SpecPower: 16
SpecAcc: 3
SpecChance: 2
SpecRecharge: 7000
SpecMagic: n
Description: Ogres are very big, strong and bad-tempered. They are slow in all meanings of the term, though, and fall easily to magic
DangerLevel: 3
}
{
Name: Gnoll
MaxHP: 15
Strength: 7
Endurance: 5
Dexterity: 6
Agility: 5
Intelligence: 4
Spirit: 3
XPYield: 12
Aggression: 7
SpecName: pounces on
SpecPower: 8
SpecAcc: 5
SpecChance: 3
SpecRecharge: 5000
SpecMagic: n
Description: A bizarre mythological beast that is part human, part hyena! If appearance was an attribute in my engine, their score would be -10...
DangerLevel: 2
}
{
Name: Crazed barbarian
MaxHP: 40
Strength: 7
Endurance: 6
Dexterity: 6
Agility: 5
Intelligence: 5
Spirit: 3
XPYield: 50
Aggression: 7
SpecName: fires an arrow at
SpecPower: 10
SpecAcc: 6
SpecChance: 4
SpecRecharge: 4000
SpecMagic: n
Description: This is a human that seems to have once been a brave adventurer, but now he's lost his mind. Be careful he doesn't take your life with it...
DangerLevel: 3
}
{
Name: Minotaur
MaxHP: 50
Strength: 8
Endurance: 8
Dexterity: 5
Agility: 3
Intelligence: 5
Spirit: 5
XPYield: 50
Aggression: 8
SpecName: breathes fire at
SpecPower: 20
SpecAcc: 5
SpecChance: 2
SpecRecharge: 6000
SpecMagic: y
Description: Half-man, half-bull, full brutish nightmare. Watch out, it's not just its temper that's fiery!
DangerLevel: 3
}
{
Name: Golem
MaxHP: 60
Strength: 9
Endurance: 9
Dexterity: 4
Agility: 1
Intelligence: 1
Spirit: 3
XPYield: 50
Aggression: 8
SpecName: fires an eye beam at
SpecPower: 15
SpecAcc: 8
SpecChance: 2
SpecRecharge: 5000
SpecMagic: n
Description: A magical creature in the shape of a very tall human, this thing is really strong and tough and nasty. It is also bound by its master, and merely beeps at your attempts to recruit it.
DangerLevel: 3
}