<I>
Name: Useless item
Description: Dunno
SellValue: 2
Use: It has no use as far as you can see
</I>
<W>
Name: Heavy club
Description: Grrreat for clobbering!
SellValue: 200
Use: ""
MinPower: 5
MaxPower: 10
Accuracy: 4
SwingTime: 2
TwoHanded: 1
DV: -2
PV: 1
</W>
<W>
Name: Scimitar
Description: A curved oriental sword, used for you-know-what
SellValue: 100
Use: 
MinPower: 1
MaxPower: 8
Accuracy: 6
SwingTime: 1
TwoHanded: 0
DV: 0
PV: 0
</W>
<W>
Name: Spear
Description: Hit 'em with the pointy end
SellValue: 50
Use: 
MinPower: 1
MaxPower: 6
Accuracy: 8
SwingTime: 1
TwoHanded: 1
DV: 2
PV: 0
</W>
<W>
Name: Dagger
Description: A small blade that you *could* use for fighting with...
SellValue: 20
Use: 
MinPower: 1
MaxPower: 4
Accuracy: 7
SwingTime: 0.6
TwoHanded: 0
DV: 0
PV: 0
</W>
<A>
Name: Leather armour
Description: Provides decent enough protection
SellValue: 300
Use: 
DV: -1
PV: 2
Weight: 4
EquippedOn: Body
</A>
<A>
Name: Iron armlet
Description: Emanates a weak magical force that reduces damage from attacks... somewhat.
SellValue: 1000
Use: 
DV: 1
PV: 1
Weight: 1
EquippedOn: Arm
</A>
<MI>
Name: Small healing potion
Description: A small bottle of healing liquid
SellValue: 500
Use: You drink the potion to recover some HP.
Effect: -10
Charges: 1
Range: 0
RechargeTime: 0
</MI>
<MI>
Name: Wand of whipping
Description: Strikes an opponent with magic!
SellValue: 500
Use: Magic
Effect: 5
Charges: 5
Range: 2
RechargeTime: 2000
</MI>
<ME>
Name: Amulet of charm
Description: Enamates charm. Said to make the wearer more trustworthy... and a bit too trusting?
SellValue: 15000
Use: Wear it
Stats: Ch, DV
Charges: 5, -3
</ME>
<W>
Name: Large spear
Description: Unimp
SellValue: 250
Use: ""
MinPower: 3
MaxPower: 6
Accuracy: 7
SwingTime: 2
TwoHanded: 1
DV: 3
PV: 0
</W>
<W>
Name: Sabre
Description: Unimp
SellValue: 125
Use: 
MinPower: 3
MaxPower: 7
Accuracy: 7
SwingTime: 1
TwoHanded: 0
DV: 1
PV: 0
</W>
<W>
Name: Hammer
Description: Unimp
SellValue: 50
Use: 
MinPower: 4
MaxPower: 7
Accuracy: 4
SwingTime: 1
TwoHanded: 1
DV: -1
PV: 0
</W>
<A>
Name: Chain mail
Description: It's sturdy
SellValue: 300
Use: 
DV: -5
PV: 5
Weight: 10
EquippedOn: Body
</A>
<MI>
Name: Wand of fireballs
Description: Strikes an opponent with magic!
SellValue: 2000
Use: Magic
Effect: 25
Charges: 2
Range: 2
RechargeTime: 4000
</MI>
<MI>
Name: Enchanted bomb
Description: Boom! And stuff
SellValue: 1500
Use: Magic
Effect: 25
Charges: 1
Range: 4
RechargeTime: 100
</MI>
<I>
Name: Priced ming vase
Description: A beautiful ming vase that is not quite priceless
SellValue: 10000
Use: You can sell it
</I>
<MI>
Name: Wand of devastation
Description: Boom! And stuff
SellValue: 10000
Use: Magic
Effect: 50
Charges: 1
Range: 4
RechargeTime: 1000
</MI>
<MI>
Name: Wand of smashing
Description: Strikes an opponent with magic!
SellValue: 1000
Use: Magic
Effect: 10
Charges: 3
Range: 2
RechargeTime: 3000
</MI>
<A>
Name: Bronze plate mail
Description: Really heavy, which has good and bad points
SellValue: 2000
Use: 
DV: -8
PV: 10
Weight: 20
EquippedOn: Body
</A>
<W>
Name: Mega sword
Description: Unimp
SellValue: 15000
Use:
MinPower: 15
MaxPower: 30
Accuracy: 7
SwingTime: 3
TwoHanded: 1
DV: -2
PV: 3
</W>
<MI>
<W>
Name: Whip of stinging
Description: Unimp
SellValue: 10000
Use: ""
MinPower: 5
MaxPower: 25
Accuracy: 10
SwingTime: 1
TwoHanded: 1
DV: 2
PV: 0
</W>
<A>
Name: Merlon's Saviour
Description: Good armour
SellValue: 20000
Use: 
DV: 4
PV: 10
Weight: 10
EquippedOn: Body
</A>
<A>
Name: Behemoth suit
Description: It's sturdy
SellValue: 15000
Use: 
DV: -8
PV: 25
Weight: 30
EquippedOn: Body
</A>
<I>
Name: Unquestionably valuable relic
Description: Dunno
SellValue: 50000
Use: You can try selling this for a small fortune!
</I>