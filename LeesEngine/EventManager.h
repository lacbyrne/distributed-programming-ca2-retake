/**

'EventManager' -- handles the running of the timer in the game,
should also handle time based events like the enemies' attacks, and the spawning of enemies around the map

**/

#ifndef TIME_MANAGER_H
#define TIME_MANAGER_H

#include "Timer.h"
//#include "..\SimpleMUD\Player.h"
#include "..\SimpleMUD\PlayerDatabase.h"
#include "Enemy.h"
#include "GameData.h"
#include "Communication.h"

using namespace SimpleMUD;

namespace LeesEngine
{
   class EventManager
   {
      public:
	     EventManager(int dummy);
		 void update();
		 string runGameEvent();

      private:
		 Timer m_timer;
		 vector<Enemy*> m_enemyManager;
		 //vector<Enemy> m_enemyManager;
		 //Spawn stuff
		 largeNum m_nextSpawnTime;
		 largeNum m_nextSpawnCounter;
		 largeNum m_nextBackupTime;
		 largeNum m_nextBackupCounter;

		 void spawnEnemy();
		 void updateEnemies(largeNum sinceLastUpdate);
   };
}

#endif