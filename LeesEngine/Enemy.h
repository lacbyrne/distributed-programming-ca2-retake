/**

'Enemy' -- models the characteristics and behaviour of enemies in the game

**/

#ifndef ENEMY_H
#define ENEMY_H

#include "Actor.h"
#include "Typedefs.h"

namespace LeesEngine
{
	//Forward declare Room for chooseNextAction function
	class Room;

	class Enemy : public Actor
	{
	    public:
			Enemy(); //Initialising data structure for enemies in Rooms requires a default constructor for enemies
			Enemy(string p_name);
			Enemy(string p_name, short p_maxHP, short currHP, short p_roomIndex, short p_x, short p_y, Attributes p_attributes,
				  int p_xpYield, short p_aggression, string p_ally, string p_specName, short p_specPower, short p_specAcc, short p_specChance,
				  int p_specRecharge, bool p_specMagic, string p_description);
			//Partial -- for constructing a 'definition' for the enemy
			Enemy(string p_name, short p_maxHP, Attributes p_attributes, int p_xpYield, short p_aggression,
				  string p_specName, short p_specPower, short p_specAcc, short p_specChance, int p_specCharge, bool p_specMagic, string p_description);
			void move(short p_direction);
			int getAttackTime();
		    short getAttackAccuracy();
		    short getAttackPower();
		    short getEvasion();
		    short getDefence();
			string getSpecName() {return m_specName;}
			bool isSpecMagic() { return m_specMagic;}
			short getSpecPower() { return m_specPower;}
			short getSpecAcc() { return m_specAcc;}
		    short checkKO();

		    bool isAlly(string p_name);
			//string chooseNextAction(LeesEngine::Room* surroundings);
			string getDescription() {return m_description;}
			void update(largeNum sinceLastUpdate); //WIP

			string getName();
			bool isEnemy(); //From Actor

			//Properties
			string& TargetName() { return m_targetName;}
			bool& IsAttackReady() { return m_attackReady;}
			string& NextAction() { return m_nextAction; }

	    private:
			string m_name; //Since this is no longer defined for actor
		    int m_xpYield;
			short m_aggression;
			string m_ally;
			string m_specName;
			short m_specPower;
			short m_specAcc;
			short m_specChance;
			int m_specRecharge;
			bool m_specMagic; //Since some special attacks are physical, not magical
			string m_description;
			largeNum m_timePassed;
			string m_targetName; //Keeping a hostile enemy's attacks focused on one particular target would making a lot of sense
			bool m_attackReady; //Because... I give up.
			string m_nextAction; //.....
			int m_nextActionTime; //Should depend on action it just used, natch
	};
}

#endif