/**

'MagicItem' -- models characteristics of items in the game that when used, evoke a particular special effect.
These may include potions (which target the player character in their effects), 
wands and scrolls (which may often target other actors in the room with their effects).

**/

#ifndef MAGIC_ITEM_H
#define MAGIC_ITEM_H

#include "Item.h"

namespace LeesEngine
{
   class MagicItem : public Item
   {
	   //One thing to bear in mind :
	   //The possibly virtual 'Use' function of Item will be virtual no more >:]
	   public:
		   MagicItem(string p_name, ItemType p_type, string p_description, int p_sellValue, string p_use,
				     /*string*/short p_effect, short p_charges, short p_range, int p_rechargeTime);
		   short getEffect() { return m_effect;}
		   short getRange() {return m_range;}
		   int getRechargeTime() { return m_rechargeTime;}

		   //Properties
		   short& Charges() {return m_charges;}

       private:
		   //string m_effect;
		   short m_effect; //Might simplify it for now by assuming the effect will always be damage related (cause or cure)
		   short m_charges;
		   short m_range;
		   int m_rechargeTime;
   };
}

#endif