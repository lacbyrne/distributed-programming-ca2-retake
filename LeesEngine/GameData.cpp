#include "GameData.h"
//#include "../SimpleMUD/Player.h"

namespace LeesEngine
{
	//Need to tell the cpp about any static attributes, otherwise compiler will complain that any that it attempts to use are 'unresolved' =P
    GameMap GameData::m_map(0);
	bool GameData::m_gameInProg;

	void GameData::setupData()
	{
        //Load the map
		loadMap();

		m_gameInProg = false;
	}

	void GameData::loadMap()
	{
		//ifstream in("map.txt");
		//If considering having each room saved and loaded separately...
		//Check if a file for room 1 exists?
		ifstream check("rooms/room1.txt");
		if(check)
		{
			//Load the map
			m_map.loadMap();
		}
		else
		{
			//Need to create it!

			//...create...
			m_map.setup();

			//...then save it
			m_map.saveMap();
		}
	}

	void GameData::saveAll()
	{
		m_map.saveMap();
		//What else?...oh wait, nothing else. =|
	}
}