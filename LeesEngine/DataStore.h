#ifndef DATA_STORE_H
#define DATA_STORE_H

//#include "Item.h"
#include "Enemy.h"
#include "Item.h"
#include "ItemIncludes.h"

#include <vector>
#include <map>

using namespace std;

namespace LeesEngine
{

class DataStore
{
   public: 
      static void setupData();
	  static void loadItems();
	  static void loadEnemies();
	  static Item* getRandomItem(bool canBeLoot); //Return a random item! [Will probably have to introduce some weightings on random item generation later]
	  static Item* getSpecificItem(string name); //Return specific item
	  static Enemy createEnemy(short dangerLevel);
      static void saveAll(); //I save...everything.
	  static Enemy getSpecificEnemy(string name);
   
   private:
      static map<string, Item*> m_itemTypes;
	  static vector<string> m_itemNames;
      static vector<vector<Enemy>> m_enemyTypes;
	  static vector<string> m_enemyNames;
};

}

#endif