#include "WieldableItem.h"

namespace LeesEngine
{
	WieldableItem::WieldableItem(string p_name, ItemType p_type, string p_description, int p_sellValue, string p_use,
			            short p_minPower, short p_maxPower, short p_accuracy, short p_swingTime, bool p_twoHanded,
						short p_DV, short p_PV, bool p_equipped)
	: Item(p_name, p_type, p_description, p_sellValue, p_use)
	{
		m_minPower = p_minPower;
		m_maxPower = p_maxPower;
		m_accuracy = p_accuracy;
		m_swingTime = p_swingTime;
		m_twoHanded = p_twoHanded;
		m_DV = p_DV;
		m_PV = p_PV;
		m_equipped = p_equipped;
	}

	short WieldableItem::getSwingTime()
	{
		return m_swingTime;
	}

	short WieldableItem::getPower()
	{
		//srand( time_t( NULL) );
		return m_minPower + rand() % (m_maxPower-m_minPower);
	}
}