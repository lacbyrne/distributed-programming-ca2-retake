/**

'Parser' -- Interface component that processes input from the player into events that alter the game state,
and communicates events that alter the game state to the player.

**/

#ifndef PARSER_H
#define PARSER_H

#include "../SimpleMUD/Player.h"
#include "Communication.h"
//#include "GameData.h"

#include <string>
#include <vector>

using namespace std;

class Enemy;
class Room;

namespace LeesEngine
{
   class Parser
   {
      public:
		   static string executePlayerInput(string command, Player& player); //Will still need to ask Game to handle some events... =P
		   static string executeEnemyCommand(string command, Enemy* enemy);
		   static string executeGameEvent(string event, Room* occursIn);

       private:
		   static string m_verbBase[];
		   static vector<string> m_inputTracker;
		   static string parseVerb(string verb);
		   static short getPreposition(string command); //...
		   static bool compoundVerb(string command);
		   static void printHelpText(Player& player);
		   static void answerQuestion(string command, Player& player);
   }; 
}

#endif