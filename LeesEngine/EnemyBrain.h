/**
Implements control for behaviour for enemies searatedly to the attributes that define them,
to prevent Enemy->Player cyclic dependency errors
**/
#ifndef ENEMY_BRAIN_H
#define ENEMY_BRAIN_H

#include "Enemy.h"
#include "../SimpleMUD/Player.h"

using namespace SimpleMUD;

namespace LeesEngine
{
   class EnemyBrain
   {
      public:

		 EnemyBrain(Enemy& enemy) : m_body(enemy)
		 {
		    
		 }
	     inline string attack(Actor& target)
		 {
	        m_body.attack(target);
		 }

		 inline string move(short p_direction)
		 {
			 m_body.move(p_direction);
		 }

		 inline string update(largeNum sinceLastUpdate)
		 {
			 m_body.update(sinceLastUpdate);
		 }

      private:
         Enemy& m_body; //Reference to 'body'
   };
}

#endif