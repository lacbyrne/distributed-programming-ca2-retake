/**

'Item' -- Models characteristics of any sort of item in the game.
Not abstract -- items that are not otherwise specially categorised, like tools and valuable items,
will/should be instances of Items.

**/

#ifndef ITEM_H
#define ITEM_H

#include <string>

using namespace std;

//Indicates the type of item, for downcasting
enum ItemType
{
	BasicItemType,
	WieldableItemType,
	ArmourType,
	MagicItemType,
	MagicEquipmentType
};

namespace LeesEngine
{
   class Item
   {
      public:
		  Item(); //Data structure for Items in Rooms requires a default constructor for Item
		  Item(string name);
		  Item(string p_name, ItemType type, string p_description, int p_sellValue, string p_use); //Item wasn't supposed to be an abstract class... ={
		  string& Name() {return m_name; }
		  ItemType getType() {return m_type; }
		  string getDescription() {return m_description; }
		  int getSellValue() {return m_sellValue; }
		  string getUse() {return m_use; }

      private:
	      string m_name;
		  ItemType m_type; //...?
		  string m_description;
		  int m_sellValue;
		  string m_use;
   };
}

#endif