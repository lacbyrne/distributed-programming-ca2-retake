/**

'MagicEquipment' -- models characteristics (and behaviour?) of other types of equipment characters can wear.
These are generally wee pieces like amulets, rings, bracelets
that cannot provide any sort of protective value (thus are not types of armour), but magical bonuses.

**/

#ifndef MAGIC_EQUIPMENT_H
#define MAGIC_EQUIPMENT_H

#include "Item.h"

#include <vector>

namespace LeesEngine
{
   class MagicEquipment : public Item
   {
       public:
		   MagicEquipment(string p_name, ItemType p_type, string p_description, int p_sellValue, string p_use,
						  vector<string> p_stats, vector<short> p_modifiers, bool p_equipped);
		   vector<string> getStats();
		   //vector<short> getModifiers(); //In practice, might only want to query the change to one stat at a time...
		   string getStat();
		   short getModifier();
		   
		   bool& isEquipped() {return m_equipped;}

       private:
		   vector<string> m_stats;
		   vector<short> m_modifiers;
		   bool m_equipped;
   };
}

#endif