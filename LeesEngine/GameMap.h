/**

'GameMap' -- a dungeon crawl / MUD game GameMap is in practice a container of the rooms.
As such, it will contain a data structure to store all rooms and functions to manipulate 'em.

**/

// WARNING ABOUT USING THIS APPROACH : 
// GameMap stores many rooms which each store many players, enemies and items,
// which can end up looking like an enormous array of Enemies and Items...
// Destructing a GameMap seems to take a considerable amount of CPU cycles,
// and creating multiple instances of GameMap [which won't be done in practice] even causes stack overflow!! >_<
// So how should this be handled...?

#ifndef GameMap_H
#define GameMap_H

#include "Room.h"

namespace LeesEngine
{
   class GameMap
   {
       public:
		   //GameMap(Array<Room> p_rooms); //?
		   GameMap( bool dummy ); //.... ????? For some reason if an object is constructed with no parameters it won't compile... ?????
		   
		   Room& getRoom(int index);
		   //void setRoom(Room& data, int index);
		   void setup();
		   Enemy& spawnEnemy(); //Try: return reference to created enemy for EventManager to track
		   void loadMap();
		   void saveMap(); //Might the latter two functions be a case of saving/loading each room individually? Might make more sense =|
		   string showRoomExits(short roomIndex);
		   short designateSpawnRoom(); //Part of workaround for enemy spawning [original plan overlooked cyclic dependency problems]
		   /*vector<string>*/vector<Enemy*> updateEnemies(largeNum timePassed);
		   vector<Enemy*> handleEnemyAttacks();
		   void updateRooms(largeNum sinceLastUpdate);

       private:
		   //Set a constant for number of rooms -- in case it should be changed later in development
		   static const int NUM_ROOMS = 8;
		   Room m_rooms[NUM_ROOMS]; //? Just assume /*34*/ 8 to be the size for now...
		   //Array<Room> m_rooms; //For now... (Is nothing going to go to Plan??? {*sigh*})
   };
}

#endif