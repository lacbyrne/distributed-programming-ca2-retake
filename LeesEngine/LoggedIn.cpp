#include "LoggedIn.h"

namespace LeesEngine
{
	LoggedIn::LoggedIn(Connection<Telnet>& p_conn, player p_player ) : thandler( p_conn )
	{
		m_player = p_player;
    }

	void LoggedIn::Handle(string p_data)
	{
		//Might need to redraw menu here from time to time, depending on inputs...?
		if(p_data.find("enter") == 0 || p_data.find("en") == 0) //'Enter' or 'en' occurs at start of the input
		{
			//output += yellow + " You shall enter the game!" + reset;
			m_player->SendString(green + " You shall enter the game!" + reset);
			//Go *to* to Game
			m_player->Conn()->AddHandler(new Game(*m_player->Conn(), m_player) );
		}
		else if(p_data.find("exit") == 0 || p_data.find("ex") == 0)
		{
			m_player->SendString(" You shall leave." + reset);
			ExitGame();
		}
		else if(p_data.find("logs") == 0 || p_data.find("l") == 0)
		{
			m_player->SendString(red + "Not yet implemented." + reset);
		}
		else if(p_data.find("highscores") == 0 || p_data.find("h") == 0)
		{
			m_player->SendString(red + "Not yet implemented." + reset);
		}
	}

	void LoggedIn::Enter()
	{
		m_player->SendString(clearscreen);

		if(!m_player->LoggedIn())
	    {
		    m_player->LoggedIn() = true;
		}

		if( m_player->Newbie() )
	    {
		    //Set up character
		    m_player->Newbie() = false; //Needs only be checked once... else character will keep being thrown back into CharacterSetup!
		    //LogoutMessage( m_player->Name() + " has transcended into the magical land of stat editing" );
            m_connection->AddHandler( new CharacterSetup( *m_connection, m_player->ID() ) );
	    }
	    else
	    {
		   loggedInMenu();
	    }

	}

	void LoggedIn::Leave()
	{
		
	}
    
	void LoggedIn::Hungup()
	{

	}

    void LoggedIn::Flooded()
	{

	}

	void LoggedIn::loggedInMenu()
	{
	   m_connection->Protocol().SendString(*m_connection, "\r\n" + yellow + "[]Enter the game " + "\r\n"
		                                   + "[]View the highscores" + "\r\n"
										   + "[]View your logs" + "\r\n"
										   + "[]Roll a new character" + "\r\n"
										   + "[]Exit the game? " + "\r\n" + "(Type 'enter' or 'exit')" + reset);
	}

	void LoggedIn::ExitGame()
	{
		//Maybe also a log or something...
		m_player->LoggedIn() = false;
	    //m_player->Active() = false;
		m_connection->Close();
	}
}