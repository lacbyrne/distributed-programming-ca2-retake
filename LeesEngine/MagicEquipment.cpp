#include "MagicEquipment.h"

namespace LeesEngine
{
	MagicEquipment::MagicEquipment(string p_name, ItemType p_type, string p_description, int p_sellValue, string p_use,
						  vector<string> p_stats, vector<short> p_modifiers, bool p_equipped)
	: Item(p_name, p_type, p_description, p_sellValue, p_use)
	{
		m_stats = p_stats;
		m_modifiers = p_modifiers;
		m_equipped = p_equipped;
	}

	vector<string> MagicEquipment::getStats()
	{
		return m_stats;
	}
}