#include "Parser.h"

using SocketLib::yellow;
using SocketLib::red;
using SocketLib::reset;
using SocketLib::green;

namespace LeesEngine
{
	//Need to init/set up verb base here...
	//string Parser::m_verbBase[29];
	string Parser::m_verbBase[] = {"attack", "buy", "chat", "collaborate", "complain", "confiscate", "destroy", "drop", "east", "equip",
		                           "examine", "exit", "get", "give", "help", "highscore", "inventory", "look", "kick", "move", "new", "north",  "offer", 
								   "pick up", "put on", "rank", "say", "sell", "score", "shout", "south", "stats", "take", "team", "use", "warp", 
								   "wear", "west", "who", "whisper", "wield"};

	string Parser::executePlayerInput(string command, SimpleMUD::Player& player)
	{
		//Answer yes or no question
		if(player.LastQueryAsked() != "")
		{
			answerQuestion(command, player);  
			return "";
		}

		short words = 1;
		string wordChecker = command;

		while(wordChecker.find(' ') != -1)
		{
			//Split!!!
			wordChecker = wordChecker.substr(0, wordChecker.rfind(' ') );
			words++;
		}

		if(getPreposition(command) == -1 && !compoundVerb(command) ) //Simple verb / noun, or else nonsense
		{
		   string verb = command.substr(0, command.find(' ') );
		   string noun = "";
		   if(words > 1)
		   {
			   noun = command.substr(command.find(' ')+1, command.length() );
		   }

		   verb = parseVerb(verb);

		   if( verb == "attack")
		   {
              if(!player.canAttack() )
		      {
			     player.SendString(red + "Not yet, you're still spinning around after your last attack! =P ");
				 return "";
		      }

			  if(noun == "")
			  {
				  player.SendString(red + "...Attack *what*?");
				  player.SendString("AVAILABLE targets: " + GameData::m_map.getRoom( player.Room() ).listEnemies() );
				  return "";
			  }

			  string targetName = BasicLib::LowerCase( noun );
			  Enemy* target = GameData::m_map.getRoom(player.Room()).findEnemy(targetName);
			  if(target != 0)
			  { 
		         //Crudely handling player->enemy combat and levelling here...
				 //It's difficult for Enemies and Players to communicate directly due to cyclic dependency
			     short playerLevel = player.Level();
				 Communication::sendRoom( player.Name() + " attacks " + target->getName(), player.Room() );
				 player.SendString(player.attack(*target));
				 //Now! How to sweep up a dead enemy? =P
				 if(target->CurrHP() < 1)
				 {
			        Enemy slain = *target;
					target = 0;
					Item* drop = DataStore::getRandomItem(true);
					GameData::m_map.getRoom(player.Room()).killEnemy(slain.getName(), drop );
					Communication::sendRoom(green + "The " + slain.getName() + " is slain!!", player.Room());

					if(drop->Name() == "loot")
					{
						short loot = 100 + rand() % 1900;
						Communication::sendRoom( BasicLib::tostring(loot) + " loot was dropped! " + player.Name() + " scooped it up.", 
							                     player.Room() );
						player.setLoot(player.getLoot() + loot, true);
					}
					else
					{
					   Communication::sendRoom(green + "A " + drop->Name() + " falls onto the floor!", player.Room() );
					}
				 }
		      }
		      else
			  {
				 //Allow players to fight...?
				 if( player.Name() == noun)
				 {
					 player.SendString("Don't beat yourself up...");
				 }
				 else if( GameData::m_map.getRoom( player.Room() ).checkForPlayer( noun ) )
				 {
					 Player& targetPlayer = *PlayerDatabase::findfull( noun );    
					 if(player.Rank() != "Regular" || targetPlayer.Rank() != "Regular")
					 {
						 player.SendString("You are not allowed to attack " + noun + "!");
					 }
					 else if( player.isAlly( noun ) )
					 {
						 player.SendString(red + "Do you really want to partake in this act of backstabbing???");
						 //Should be a choice, where answering yes drains the attacker's charisma and removes the target as an ally
						 //...But I have more important things to do with the time left =|
					 }
					 else
					 {
						 player.SendString("You attack " + noun + "!");
						 targetPlayer.SendString(red + player.Name() + " attacks you!!");
						 string attackFeedback = player.attack( targetPlayer );
						 player.SendString( attackFeedback);
						 targetPlayer.SendString( attackFeedback);
					 }
				 }
				 else
				 {
					 player.SendString("Who, or where, is " + noun + "?!");
				 }
			  }
		   }
		   else if(verb == "buy")
		   {
			   if(noun == "")
			   {
			      player.SendString("Buy what?");
				  player.SendString( GameData::m_map.getRoom( player.Room() ).listItems() );
			   }
			   else
			   {
			      string purchaseFeedback = GameData::m_map.getRoom( player.Room() ).attemptPurchase(noun);
			      if(purchaseFeedback != "")
			      {
			         player.LastQueryAsked() = purchaseFeedback;
					 player.SendString(purchaseFeedback);
			      } 
			      else
			      {   
			         player.SendString("There is no " + noun + " to buy!");
			      }
			   }
		   }

		   else if(verb == "chat" || verb == "say")
		   {
		      Communication::sendRoom(yellow + player.Name() + " says: " + noun, player.Room() );
		   }

		   else if(verb == "collaborate") //Should be collaborate *with*, if done properly, but allow it...
		   {
			   if( GameData::m_map.getRoom( player.Room() ).checkForPlayer(noun) )
			   {
				   if(player.Name() != noun) //How did I need see that one coming... ={
				   {
					   player.collaborateWith( *PlayerDatabase::findfull(noun) );
				   }
				   else
				   {
					   player.SendString("I know they say 'you must learn to accept yourself first', but that pushes it...");
				   }
			   }
			   else
			   {
				   player.SendString(red + "Where is " + noun + "?");
			   }
		   }

		   else if(verb == "equip" )
		   {
		      if(noun == "")
			  {
				  player.SendString(" Equip what?");
				  player.SendString( player.showInventory() );
			  }
			  else
			  {
				  //BasicItems can't really be equipped... this is just saying 'equip anything'
				  player.SendString(player.setEquipped( noun, ItemType::BasicItemType)); 
			  }
		   }

		   else if(verb == "examine") //WIP; will be expanded to include items too
	       {
			  if(noun == "")
		      {
		         player.SendString(red + "...examine *what*?");
				 player.SendString(GameData::m_map.getRoom( player.Room() ).listEnemies() );
				 return "";
		      }

			  Enemy* e = GameData::m_map.getRoom( player.Room()).findEnemy(noun);
			  if(e != 0)
			  {
			     player.SendString( e->getDescription() );
			  }
			  else
			  {
				 player.SendString("What " + noun + "...?");
			  }
		   }

		   else if( verb == "exit" )
		   {
		      player.SendString("See ya!");
			  return "exit";
		   }

		   else if( verb == "get") //or pick up
		   {
			  if(noun == "")
			  {
			     player.SendString("...get *what*?");
			     //Help 'em out by showing a list!
			     player.SendString("\r\nAvailable items: \r\n" + GameData::m_map.getRoom( player.Room() ).listItems() );
			  }
			  else
			  {
				  if( GameData::m_map.getRoom(player.Room()).getType() == "shop")
				  {
					  player.SendString(yellow + 
				      "A heavily-armoured shopkeeper with an 'eight pack' and an enormous halberd *persuades* you to use the 'buy' command instead");
				  }
				  else
				  {
		             player.SendString(player.acquireItem( GameData::m_map.getRoom(player.Room()).getItem(noun) ) );
				  }
			  }
		   }

		   else if( verb == "help" || parseVerb(noun) == "help")
		   {
			   printHelpText(player);
		   }

		   else if( verb == "highscore" || verb == "score")
		   {
			   Communication::showScores(&player);
		   }

		   else if (verb == "inventory" || parseVerb(noun) == "inventory")
		   {
			   player.SendString( yellow + player.showInventory() + reset);
		   }

		   else if( verb == "kick")
		   {
			   if(player.Rank() == "overlord" || player.Rank() == "admin")
			   {
				  string reason = "";
				  if(words == 1)
				  {
					  player.SendString("...Kick whom?"); 
					  player.SendString(Communication::getWhoList() );
				  }
				  else if(words > 2)
				  {
				     string parsedCommand = noun.substr(0, noun.length() );
					 reason = parsedCommand.substr(parsedCommand.find(' ')+1, parsedCommand.length() );
					 noun = parsedCommand.substr(0, parsedCommand.find(' '));
				  }

				  if(noun == player.Name() )
				  {
					  player.SendString("Don't kick yourself!");
				  }
			      else if( PlayerDatabase::hasfull( noun ) )
				  {
					 Player& target = *PlayerDatabase::findfull( noun );
					 if( target.isActive() )
					 {
					    if( target.Rank() == "admin" || (target.Rank() == "overlord" && player.Rank() == "overlord") )
					    {
					       player.SendString(red + target.Name() + " can't be kicked!");
					    }
					    else
					    {
						   string output = player.Name() + " kicks you out";
						   if(reason != "")
						   {
						      output += " for " + reason;
						   }
						   output += "!!";
					       target.SendString(output);
					       target.Conn()->Close();
						}
					 }
					 else
					 {
						 player.SendString(red + target.Name() + " isn't even logged in!");
					 }
				  } 
				  else
				  {
				     player.SendString(red + noun + " doesn't exist!");
				  }
			   }
			   else
			   {
				   player.SendString(red + " You don't have the power to do that!");
			   }
		   }

		   else if(verb == "look")
	       {
		      player.SendString(yellow + GameData::m_map.getRoom(player.Room()).getRoomInfo() + "\r\n" + 
			                           GameData::m_map.showRoomExits(player.Room()) );
	       }

		   else if( verb == "north" || parseVerb(noun) == "north" 
			     || verb == "south" || parseVerb(noun) == "south"
			     || verb == "west" || parseVerb(noun) == "west"
			     || verb == "east" || parseVerb(noun) == "east" )
	       {
	          short priorRoom = player.Room();  
  
	          //Expanding on moving stuff a bit: consider tracking player position before and after moving,
	          //and if it stays the same conclude that the player couldn't move!
			  if( verb == "north" || parseVerb(noun) == "north"  )
	          {
	             player.move(1);
	          }
	          else if( verb == "south" || parseVerb(noun) == "south" )
	          {
	             player.move(2);
	          }
	          else if( verb == "west" || parseVerb(noun) == "west" )
	          {
		         player.move(3);
	          }
	          else if( verb == "east" || parseVerb(noun) == "east" )
	          {
 		         player.move(4);
	          }

	          if(player.Room() != priorRoom)
	          {
				 Communication::sendRoom(yellow + player.Name() + " leaves the room", priorRoom);
			     player.SendString(yellow + GameData::m_map.getRoom(player.Room()).getRoomInfo() + "\r\n" + 
			                           GameData::m_map.showRoomExits(player.Room()) );
				 //For this, should probably do a 'send to others' function that skips the player
				 Communication::sendRoom(yellow + player.Name() + " enters the room", player.Room());
	          }
			  else
			  {
				  player.SendString(red + "You can't go that way!" + reset 
					                + "\r\nPOSSIBLE exits: " + GameData::m_map.showRoomExits(player.Room()) );
			  }
	       }

		   else if(verb == "rank")
		   {
			   if(player.Rank() == "admin")
			   {
				  string rank = "";

				  if(words == 1)
				  {
					  player.SendString("...Rank whom?");
					  player.SendString( Communication::getWhoList() );
				  }
                  else if(words == 2) //Just 'rank' and name given: promote to Overlord
				  {
					  rank = "overlord";
				  }
				  else
				  {
					  string parsedCommand = noun;
					  rank = parsedCommand.substr(parsedCommand.find(' ')+1, parsedCommand.length() );
					  noun = parsedCommand.substr(0, parsedCommand.find(' '));
					  cout << rank;

					  if(rank == "noob" || rank == "regular" || rank == "overlord")
					  {
					       
					  }
					  else
					  {
						  player.SendString( red + "Invalid command!" );
						  return "";
					  }
				  }

				  if(noun == player.Name())
				  {
					  player.SendString("You can't demote yourself!");
				  }
				  else if( PlayerDatabase::hasfull( noun ) )
				  {
					 Player& target = *PlayerDatabase::findfull( noun );
					 target.Rank() = rank;
					 target.SendString(yellow + player.Name() + " has changed your rank to " + rank);
					 player.SendString(target.Name() + " is now a " + rank);
				  }
				  else
				  {
					  player.SendString(red + "You can't find " + noun);
				  }
			   }
			   else
			   {
				   player.SendString(red + "You don't have the authority to do that!");
			   }
		   }

		   else if(verb == "sell")
		   {
			   if( GameData::m_map.getRoom( player.Room() ).getType() != "shop")
			   {
			      //Actually, there's no reason not to attempt to sell items to other players... is there?? =}
				  //Maybe later.
				   player.SendString("There doesn't seem to be anyone around who is interested in your offer.");
			   }
			   else
			   {
				   Item* item = player.getItem(noun, false);
				   if(item != 0)
				   {
					   player.LastQueryAsked() = GameData::m_map.getRoom( player.Room() ).attemptSale( item, player.getCharisma(), false );
					   player.SendString( player.LastQueryAsked() );
				   }
				   else
				   {
					   player.SendString( "You don't have a " + noun + " to sell!" );
				   }
			   }
		   }

		   else if(verb == "stats" || parseVerb(noun) == "stats")
		   {
			   player.SendString( yellow + player.showStats() + reset);
		   }

		   else if(verb == "team" || parseVerb(noun) == "team")
		   {
			   player.SendString( player.showAllies() );
		   }

		   else if(verb == "use")
		   {
			   Item* item = player.getItem( noun, false);
			   if(item != 0)
			   {
				   player.useItem(item, 0);
			   }
			   else
			   {
				   player.SendString("You don't have a " + noun + "!");
			   }
		   }

		   else if(verb == "warp")
		   {
			   if(player.Rank() != "regular")
			   {
				   player.SendString("Bing! You warp back to the first room.");
				   player.moveAbs(0);
			   }
			   else
			   {
				   player.SendString(red + "...You can't do that.");
			   }
		   }

		   else if(verb == "wear")
		   {
		      if(noun == "")
		      {
		         player.SendString(red + "...Wear *what*?");
		      }
			  else
			  {
				  player.SendString(player.setEquipped(noun, ItemType::ArmourType));
			  }
		   }

		   else if(verb == "who") //Who's online?
		   {
			   player.SendString( Communication::getWhoList() );
		   }

		   else if(verb == "wield")
	       {
			  if(noun == "")
		      {
		         player.SendString(red + "...Wield *what*?");
		      }
			  else
			  {
			     player.SendString(player.setEquipped(noun, ItemType::WieldableItemType));
			  }
		   }

		   else
		   {
			   player.SendString(red + "Unknown command.");
		   }
		}

		else if( getPreposition(command) != -1 )
		{
			string verb = command.substr(0, getPreposition(command) );
			command = command.substr(getPreposition(command), command.length() );
			string noun = command.substr(command.find(' ')+1, command.length() );
			cout << "verb:  " << verb << " \t\t noun: " << noun << endl;
		}

		//feedback = "Computer says: " + command;
		return "";
	}

	//Converts whatever part of input we have into a verb
	string Parser::parseVerb(string verb)
	{
	   string toReturn;
       short i = 0;
	   short j = 0;
	   bool mismatch = false;
	   
	   //**Have to be careful that the limit used is neither greater than nor less than the size of the verb base!!
	   //  Pity that C arrays have no length member... =|
	   for(i = 0; i < 42; i++)
	   {
		   if(verb == m_verbBase[i])
		   {
		      return m_verbBase[i];
		   }
		   else
		   {
			   mismatch = false;

			   for(j = 0; j < verb.length(); j++)
			   {
				   if(verb[j] != m_verbBase[i][j])
				   {
					   mismatch = true;
					   break;
				   }
			   }

			   if(!mismatch)
			   {
				   return m_verbBase[i];
			   }
		   }
	   }

	   return "";
	}

	void Parser::answerQuestion(string command, Player& player)
	{
	   if(player.LastQueryAsked().find("buy") != -1)
	   {
	      if(command == "yes" || command == "y")
	      {
		     string itemName = player.LastQueryAsked();
			 itemName = itemName.substr( player.LastQueryAsked().find("buy the")+8, itemName.length() );
			 itemName = itemName.substr(0, itemName.find(','));
			 cout << itemName;
		     Item* item = GameData::m_map.getRoom(player.Room()).getItem(itemName);
			 if(item->getSellValue() > player.getLoot() )
			 {
			    player.SendString(red + "You can't afford that! That shopkeeper rolls his eyes and puts it back in the shelf.");
				//...?
				GameData::m_map.getRoom(player.Room()).addItem(item);
			 }
			 else
			 {
			    player.setLoot(player.getLoot() - item->getSellValue(), true);
			    player.acquireItem(item);
				player.SendString("You bought a " + itemName + " for " + BasicLib::tostring(item->getSellValue()) );
				Communication::sendRoom(player.Name() + " buys a " + itemName, player.Room());
		     }
		  }
		  else 
		  {
		     player.SendString("...You decline.");
		  }
	   }
	   else if(player.LastQueryAsked().find("sell") != -1)
	   {
	      if(command == "yes" || command == "y")
		  {
		     //Take item out of player's inventory
			 string itemName = player.LastQueryAsked();
			 itemName = itemName.substr(0, player.LastQueryAsked().find('.') );
			 itemName = itemName.substr(player.LastQueryAsked().find("your")+5, itemName.length() );
			 Item* item = player.getItem(itemName, true);

			 string saleFeedback = GameData::m_map.getRoom( player.Room() ).attemptSale(item, player.getCharisma(), true);
			 player.SendString( saleFeedback );
			 
			 //Get loot acquired
			 saleFeedback = saleFeedback.substr( saleFeedback.find("for ")+4, saleFeedback.length() );
			 saleFeedback = saleFeedback.substr(0, saleFeedback.find(" loot") );
			 short lootGain = atoi(saleFeedback.c_str() );
			 player.setLoot( player.getLoot() + lootGain, true );
		  }
		  else
		  {
			  player.SendString(" You refuse the offer.");
		  }
	   }
	   else if(player.LastQueryAsked().find(" collaborate ") != -1)
	   {
		   string playerName = player.LastQueryAsked().substr(0, player.LastQueryAsked().find(' ') );
		   Player& otherPlayer = *PlayerDatabase::findfull(playerName);

	      if(command == "yes" || command == "y")
		  {
			  player.SendString("You accept. " + green + playerName + " joins your team!");
			  otherPlayer.SendString(green + player.Name() + " has joined your team! ^_^");
			  player.addAlly(/*playerName*/otherPlayer);
			  otherPlayer.addAlly(/*player.Name()*/player);
		  }
		  else
		  {
			  otherPlayer.SendString(red + "..." + player.Name() + " declined... =*0( ");
			  player.SendString("...You decline.");
		  }
	   }
	   else if(player.LastQueryAsked().find(" target "))
	   {
		   Enemy* e = GameData::m_map.getRoom( player.Room() ).findEnemy( command );

		   if(e != 0)
		   {
		      short itemStringOffset =  player.LastQueryAsked().find("your")+5;
		      string itemString = player.LastQueryAsked().substr(itemStringOffset, player.LastQueryAsked().length() );
		      itemString = itemString.substr(0, itemString.find('.'));
		      Item* item = player.getItem( itemString, false);
			  
			  player.useItem( item, e);
		   }
		   else
		   {
			   player.SendString(" What " + command + "?");
		   }
	   }

	   player.LastQueryAsked() = "";
	}

	string Parser::executeEnemyCommand(string command, Enemy* player)
	{
		string feedback;

		return feedback;
	}

	string Parser::executeGameEvent(string event, Room* occursIn)
	{
		string feedback;

		return feedback;
	}

	short Parser::getPreposition(string command)
	{
		if( (command.find(" to ") != -1) || (command.find( " from ") != -1)  ) //What others...?
		{
			return true;
		}

		if( command.find(" to ") != -1)
		{
			return command.find(" to ");
		}
		if( command.find(" from ") != -1)
		{
			return command.find(" from ");
		}
		if(command.find(" with ") != -1)
		{
			return command.find(" with ");
		}

		return -1;
	}

	bool Parser::compoundVerb( string command)
	{
		if( (command.find(" up ") != -1) || (command.find( " with " ) != -1) || (command.find(" on ") != -1) )
		{
		    return true;
		}

		return false;
	}

	void Parser::printHelpText(Player& player)
    {
	   //Note that C strings are kind of odd in that just " " denotes a const char*, and two of these cannot be concatenated...
	   //So, if concatenation is to used (for readability, or whatnot), a string is the way to go!
	   string output = " ====================COMMANDS THUS FAR==================== \r\n ";
	   output += " attack <enemy or player name> : attack an enemy or player\r\n";
	   output += " buy <item> : gets an item from a shop in exchange for loot\r\n";
       output += " chat <message>: communicates a message to all players in the room\r\n";
	   output += " collaborate <player>: request that a player teams up with you\r\n";
	   output += " equip <item> : sets an item to be worn or wielded \r\n";
	   output += " examine <enemy> : looks at an enemy\r\n";
	   output += " exit : exits the realm. Progress will be saved.\r\n";
       output += " get <item> OR pick up <item> : get an item lying in a room\r\n";
	   output += " (high)score : display the leaderboard ";
	   output += " inventory: display your inventory\r\n";
	   output += " look : call up room description \r\n";
	   output += " north, south, east, west : move in one of those directions \r\n";
	   output += " sell <item>: sell an item if in a shop";
	   output += " stats : show stats \r\n ";
	   output += " team : display all your allies \r\n"; 
	   output += " wear : put on armour\r\n";
	   output += " wield <item> : set a wieldable item to be wielded \r\n";
	   output += " \r\n====================RANK RESTRICTED==================== \r\n ";
	   output += " kick <player> : kick a player out of the game [Overlords and admins only]";
	   output += " kick <player> <reason> : kick a player out of the game with a reason given [Overlords and admins only]";
	   output += " rank <player>: promote a user to the Overlord rank [admin only]";
	   output += " rank <player> <rank>: change a player's rank to a specified one [admin only]";
	   output += " warp : go back to the first room [all but Regular users ";
	   output += " \r\n======================================================= \r\n ";
	   output += "[[NOTE ON PARSER : partial inputs will be interpreted as the next possible command in alphabetical order]]\r\n";
	   output += "==============================================\r\n";
	   player.SendString(output);
    }
}