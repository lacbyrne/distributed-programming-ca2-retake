#include "CharacterSetup.h"

namespace LeesEngine
{
	void CharacterSetup::Handle( string p_data )
	{
		if(m_state == StatIncreaser)
		{
			//Stat increaser
			short input = atoi(p_data.c_str());
			if( input > 0 && input < 8)
			{
				switch(input)
				{
				   case 1:
				   m_player->m_attributes.strength += 1;
				   break;

				   case 2:
				   m_player->m_attributes.endurance += 1;
				   break;

				   case 3:
				   m_player->m_attributes.dexterity += 1;
				   break;

				   case 4:
				   m_player->m_attributes.agility += 1;
				   break;

				   case 5:
				   m_player->m_attributes.intelligence += 1;
				   break;

				   case 6:
				   m_player->m_attributes.spirit += 1;
				   break;

				   case 7:
				   m_player->m_attributes.charisma += 1;
				   break;
				}

				m_player->StatBonuses() -= 1;
			}

			if(m_player->StatBonuses() == 0 || p_data == "quit" || p_data == "exit")
			{
				//(For now) Player's max HP is a function of their endurance
				//...keep it to f(x) = 3x for now?
				m_player->MaxHP() = m_player->m_attributes.endurance * 3;
				m_player->CurrHP() = m_player->MaxHP();
				m_player->SendString("Stats set up! Now entering the game...");
				m_player->Conn()->RemoveHandler();
			}
		}
		else if(m_state == RaceChoice)
		{
			string race = BasicLib::LowerCase(p_data);
			bool validInput = false;

			if(race == "human" ||  race[0] == 'h')
			{
				m_player->Race() = Races::human;
				validInput = true;
			}
			else if(race == "troll" ||  race[0] == 't')
			{
				m_player->Race() = Races::troll;
				validInput = true;
			}
			else if(race == "elf" ||  race[0] == 'e')
			{
				m_player->Race() = Races::elf;
				validInput = true;
			}
			else
			{
				m_player->SendString("...What?");
			}

			if(validInput)
			{
				setInitialStats();
				m_state = StatIncreaser;
			}
		}

		printSetupMenu();
	}

    void CharacterSetup::Enter()
	{
		if(m_player->m_attributes.strength == 0) //Newly created
		{
			m_state = RaceChoice;
			m_player->SendString(yellow + "Set up your character. Now!" + reset);
		}
		else
		{
			m_state = StatIncreaser;
		}

		printSetupMenu();
	    //To be continued

	}

	void CharacterSetup::Leave()
	{
		m_player->SendString("You leave stat editing");
	}

	void CharacterSetup::Hungup()
	{

	}

	void CharacterSetup::Flooded()
	{

	}

	void CharacterSetup::printSetupMenu()
	{
		if(m_state == StatIncreaser)
		{
		   m_player->SendString(yellow + "Pick a stat to increase: " + "\r\n" + 
			                 "1/Strength: " + BasicLib::tostring(m_player->m_attributes.strength) + "\r\n" +
			                 "2/Endurance: " + BasicLib::tostring(m_player->m_attributes.endurance) + "\r\n" +
							 "3/Dexterity: " + BasicLib::tostring(m_player->m_attributes.dexterity) + "\r\n" +
							 "4/Agility: " + BasicLib::tostring(m_player->m_attributes.agility) + "\r\n" +
							 "5/Intelligence: " + BasicLib::tostring(m_player->m_attributes.intelligence) + "\r\n" +
							 "6/Spirit: " + BasicLib::tostring(m_player->m_attributes.spirit) + "\r\n" +
							 "7/Charisma: " + BasicLib::tostring(m_player->m_attributes.charisma) + "\r\n" +
							 "< stat points left to spend: " + BasicLib::tostring(m_player->StatBonuses()) + ">");
		}
		else if(m_state == RaceChoice)
		{
			m_player->SendString(yellow + "[]Choose a race: " + "\r\n" + "[]Human " + "\r\n" + "[]Troll " + "\r\n" + "[]Elf " + reset);
		}
	}

	void CharacterSetup::setInitialStats()
	{
		//All initial stats to be 3?
		//short initialStats = 3;
		//Add randoms
		initStat(m_player->m_attributes.strength);
		initStat(m_player->m_attributes.endurance);
		initStat(m_player->m_attributes.dexterity);
		initStat(m_player->m_attributes.agility);
		initStat(m_player->m_attributes.intelligence);
		initStat(m_player->m_attributes.spirit);
		initStat(m_player->m_attributes.charisma);

		//Race modifiers
		if(m_player->Race() == Races::troll)
		{
			m_player->m_attributes.strength += 4;
			m_player->m_attributes.agility -= 2;
			m_player->m_attributes.intelligence -= 3;
			m_player->m_attributes.spirit -= 2;
			m_player->m_attributes.charisma -= 3;

			//Give 'em an item or two to start with? At least to test what's been done so far
			m_player->acquireItem( DataStore::getSpecificItem("heavy club") );
		}
		else if(m_player->Race() == Races::elf)
		{
			m_player->m_attributes.strength -= 2;
			m_player->m_attributes.endurance -= 2;
			m_player->m_attributes.agility += 2;
			m_player->m_attributes.intelligence += 1;
			m_player->m_attributes.spirit += 1;
			m_player->m_attributes.charisma += 1;

			m_player->acquireItem( DataStore::getSpecificItem("dagger") );
			m_player->acquireItem( DataStore::getSpecificItem("iron armlet") );
			m_player->acquireItem( DataStore::getSpecificItem("wand of whipping") );
		}
		else if(m_player->Race() == Races::human)
		{
			//No change to stats; they're pretty much 'standard'
			m_player->acquireItem( DataStore::getSpecificItem("scimitar") );
			m_player->acquireItem( DataStore::getSpecificItem("leather armour") );
			m_player->acquireItem( DataStore::getSpecificItem("small healing potion") );
		}
	}

	void CharacterSetup::initStat(short& stat)
	{
		stat = 3 + rand() % 4; // + 0-3
	}
}