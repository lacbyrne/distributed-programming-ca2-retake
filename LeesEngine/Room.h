/**

'Room' -- models characteristics of a room in the game.
It is currently proposed that the room would be a complex structure
that would allow enemies and players to walk around within the room,
and the room would have to track the players, enemies and items in the room, their states and positions.

**/

#ifndef ROOM_H
#define ROOM_H

//#include "HashTable.h"
//#include "Array.h"
#include "Enemy.h"
#include "Item.h"

#include <string>
#include <fstream>
#include <vector>

#include "../BasicLib/BasicLib.h" //Using tostring for some int->string parsing

using namespace std;

//class Actor;
//class Enemy;
//class Item;

namespace LeesEngine
{
   struct Exits
   {
      short north;
	  short south;
	  short west;
	  short east;
   };

   class Room
   {
       public:
		   Room(); //Initialising Room array in Map requires a default constructor for rooms

		   /*~Room()
		   {
		      m_players.~Array();
		      m_enemies.~Array();
		      m_items.~Array();
		   }*/

		   Room(short p_roomIndex, string p_roomName, string p_roomType, short p_dangerLevel, 
			    /*Array<string> p_players, Array<Enemy> p_enemies, Array<Item> p_items,*/
				Exits p_exits, string p_description);
		   //Room(short p_roomIndex, string p_roomType, short p_dangerLevel, Exits p_exits, string p_description);
		   Room(string p_description);
		   string drawRoom();
		   string isSpaceOccupiable(short x, short y);
		   Actor& findTargetOn(short x, short y);
		   Item getItemOn(short x, short y);
		   string getDescription();
		   Exits getExits();
		   string getType() {return m_roomType;}
		   void addItem(Item* item);
		   //...
		   //Item getItem(int index);
		   void load(short roomIndex);
		   void save();
		   Enemy* addEnemy(Enemy e);
		   void killEnemy(string name, Item* drop);
		   Item* getItem(string name);
		   /*string*/vector<Enemy*> updateEnemies(largeNum timePassed);
		   void addPlayer(string name);
		   void removePlayer(string name);
		   bool checkForPlayer(string name);
		   string listItems(); //Should come in useful if the player wants to get *an* item
		   string listEnemies(); 
		   string attemptPurchase(string itemName);
		   string attemptSale(Item* item, short charisma, bool confirmed);
		   string showInhabitants(); //Experimental =P
		   string getRoomInfo();
		   vector<Enemy*> getAttackingEnemies();
		   Enemy* findEnemy(string name);
		   void update(largeNum sinceLastUpdate);

		   //Properties
		   string& RoomName() {return m_roomName;}
		   short& DangerLevel() {return m_dangerLevel;}
		   vector<string> getPlayers();
		   bool& DontUpdate() { return m_dontUpdate; }

       private:
		   short m_roomIndex;
		   string m_roomName; //Useful, maybe? =P
		   string m_roomType;
		   short m_dangerLevel;
		   //Values are hashed, stored and retrieved, based on their positions
		   /*HashTable<short, string> m_players;
		   HashTable<short, Enemy> m_enemies;
		   HashTable<short, Item> m_items;*/
		   //\-->Are hash tables worth it, to store 25 bits of data per structure??
		   //Will try with dynamic arrays instead...
		   /*Array<string> m_players;
		   Array<Enemy> m_enemies;
		   Array<Item> m_items;*/
		   //string m_players[25]; //Not-so-dynamic arrays now... {*sigh*} strange access violation errors
		   vector<string> m_players;
		   //Enemy m_enemies[25];
		   vector<Enemy> m_enemies; //No point using 2D based approaches until 2D is implemented!
		   //Item m_items[25];
		   vector<Item*> m_items;
		   //short[4] m_exits;
		   Exits m_exits;
		   string m_description;
		   //[[Trying to fix crash caused when attempting to update list that enemy is being taken out of
		   bool m_dontUpdate;
		   largeNum m_timePassed; //For special rooms with time based events
   };
}

#endif