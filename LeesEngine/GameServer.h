//Runs the ListeningManager and ConnectionManager for the MUD/game

#include "../SocketLib/SocketLib.h"
#include "../SimpleMUD/Logon.h"

using namespace SocketLib; //*** DON'T FORGET THE NAMESPACES!! {*sigh*}
using namespace SimpleMUD;

namespace LeesEngine
{
    class GameServer
	{
	   public:
		   GameServer(short* ports, short numOfPorts); //Passes in list of ports to listen on
		   void runServer();

	   private:
		   ListeningManager<Telnet, Logon> m_lm;
           ConnectionManager<Telnet, Logon> m_cm;
	};
}