#include <string>

#include "../SocketLib/SocketLib.h"
#include "../SimpleMUD/Player.h"
#include "GameData.h"
//#include "Logs.h"

#include "CharacterSetup.h"
#include "../SimpleMUD/Game.h"

using std::string;
using namespace SocketLib;
using namespace SimpleMUD;

namespace LeesEngine
{
    #ifndef LOGGEDIN_H
    #define LOGGEDIN_H

    class LoggedIn : public Telnet::handler
	{
		typedef Telnet::handler thandler;

	    public:
		   LoggedIn( Connection<Telnet>& p_conn, player p_player);
		   void Handle(string p_data);
		   void Enter();
		   void Leave();
		   void Hungup();
		   void Flooded();
		   void ExitGame();

	     private:
            player m_player;
			void loggedInMenu(); //Display text for the menu for this handler
	};

    #endif
}