#include "GameServer.h"

namespace LeesEngine
{
	GameServer::GameServer(short ports[], short numOfPorts) : m_cm(128, 60, 65536)
	{
       m_lm.SetConnectionManager( &m_cm );
       short i = 0;
	   for(i = 0; i < numOfPorts; i++)
	   {
		   m_lm.AddPort( ports[i] );
	   }
	}

	void GameServer::runServer()
	{
		m_lm.Listen();
        m_cm.Manage();
		ThreadLib::YieldThread();
	}
}