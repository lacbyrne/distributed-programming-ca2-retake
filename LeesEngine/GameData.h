/**

'GameData' -- retrieves, stores and saves volatile data for the playing session.
Also stores the data for the attributes used to construct particular instances of items and enemies,
like halberds, plate armours, minotaurs, berserk behemoths, 
Members so they can be easily available to other classes.

**/

#ifndef GAME_DATA_H
#define GAME_DATA_H

#include <ctime>

#include "GameMap.h"

#include <fstream>

namespace LeesEngine
{
   class GameData
   {
       public:
	       static void setupData();
		   static void loadMap();
		   static void saveAll(); //I save...everything.
		   static GameMap m_map;
		   static bool m_gameInProg; // ...Don't ask.

       //private: //Of course nothing is private! It *is* 'global' data... =P
   };
}

#endif