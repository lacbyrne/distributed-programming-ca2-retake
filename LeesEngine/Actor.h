/**

'Actor' -- abstract class that provides common attributes and functions to playable characters and enemies.

**/

#ifndef ACTOR_H
#define ACTOR_H

#include <string>
#include <ctime>

#include "../BasicLib/BasicLib.h"

using namespace std;

namespace LeesEngine
{
   struct Attributes
   {
	   short strength;
	   short endurance;
	   short dexterity;
	   short agility;
	   short intelligence;
	   short spirit;
	   short charisma;
   };

   class Actor
   {
      public:
		virtual void move(short p_direction) {};
		string attack(Actor& target);
		string specialAttack(Actor& target, short specAcc, short specPower);
		string handleDamagePhase(Actor& target, short damage); //This phase is common to both physical attack and special attack
		virtual int getAttackTime() {return 0;}
		virtual short getAttackAccuracy() {return 0;}
		virtual short getAttackPower() {return 0;}
		virtual short getEvasion() {return 0;}
		virtual short getDefence() {return 0;}
		virtual short checkKO() {return 0;}
		virtual bool isAlly(string p_name) {return false;}
		virtual void winBattle(short spoils, bool isXP) {};
		virtual string getName();
		virtual bool isEnemy() {return false;} //Yes... desparation really does set in

		short getStrength() {return m_attributes.strength;}
		short getDexterity() {return m_attributes.dexterity;}
		short getIntelligence() {return m_attributes.intelligence;}
		short getSpirit() {return m_attributes.spirit;}

		//Properties
		short& MaxHP() {return m_maxHP;}
		short& CurrHP() {return m_currHP;}
		short& Room() {return m_roomIndex;}

		Attributes m_attributes; //...public?

      protected:
		Actor(); //A default constructor for Enemy will require a default constructor for Actor... =P
		//Actor(string p_name, short p_HP, short* p_attributes, short p_roomIndex, short p_x, short p_y);
		Actor(string p_name, short p_maxHP, short p_currNo, /*shortAttributes p_attributes,*/ short p_roomIndex, short p_x, short p_y);
	    //string m_name;
		short m_maxHP;
		short m_currHP;
		//short m_attributes[7]; //strength, endurance, dexterity, agility, intelligence, spirit, charisma
		short m_roomIndex;
		short m_x;
		short m_y;
   };
}

#endif ACTOR_H