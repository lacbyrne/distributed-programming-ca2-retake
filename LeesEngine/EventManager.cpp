#include "EventManager.h"

using SocketLib::red;

namespace LeesEngine
{
   EventManager::EventManager(int dummy) : m_timer(0)
   {
	   m_nextSpawnCounter = 0.0;
	   m_nextSpawnTime = 10000; //15 seconds average [will be influenced by random factors]
	   m_nextBackupCounter = 0;
	   m_nextBackupTime = /*300000*/10000; //5 minutes?

	   srand( time(NULL) );
   }

   void EventManager::update()
   {
      m_timer.getTimeElapsed();
	  largeNum sinceLastUpdate = m_timer.getSinceLastUpdate();

	  if(sinceLastUpdate < 0) //Timer not yet initialised!
	  {
	     
	  }
	  else
	  {
         //Update all the active players!
	     operate_on_if( PlayerDatabase::begin(),
                        PlayerDatabase::end(),
				        playerupdate( sinceLastUpdate ),
                        playeractive() );

		 //Update enemies
		 updateEnemies(sinceLastUpdate);

		 GameData::m_map.updateRooms(sinceLastUpdate);

	     //Need to run events as well... and stuff
         m_nextSpawnCounter += sinceLastUpdate;	  

		 if( (m_nextSpawnCounter > m_nextSpawnTime) && Communication::findAnActivePlayer() ) //**Needs an extra condition that checks if *any* player is active! But how to do this...?
	     {
		    m_nextSpawnCounter = 0;
		    //Spawn!
			spawnEnemy();
		    
		    m_nextSpawnTime = 10000 + (rand() % 15000); //10 to 25 seconds? 
			//m_nextSpawnTime = 10000;
	     }
		 
	     m_nextBackupCounter += m_timer.getSinceLastUpdate();

		 if(m_nextBackupCounter >= m_nextBackupTime)
		 {
			 m_nextBackupCounter = 0;
			 GameData::saveAll();
		 }
	  }
   }

   string EventManager::runGameEvent()
   {
      //m_timer.getTimeElapsed();

	  //else
	  return "";
   }

   void EventManager::spawnEnemy()
   {
	   short spawnRoomIndex = GameData::m_map.designateSpawnRoom();
	   if(spawnRoomIndex >= 0)
	   {
		    //GameData::m_map.spawnEnemy();
		    short spawnDL = GameData::m_map.getRoom(spawnRoomIndex).DangerLevel();
			Enemy e = DataStore::createEnemy( spawnDL);
			GameData::m_map.getRoom(spawnRoomIndex).addEnemy(e);
		    //m_enemyManager.push_back( (GameData::m_map.getRoom(spawnRoomIndex).addEnemy(e)) );

			//For now, will tell all the players in the room about the spawned enemy
			//[[Should really find a less 'lazy' way to do this... maybe? @_@ ]]
			string notif = "Oh no! A " + e.getName() + " just popped up!";
			Communication::sendRoom(red + notif, spawnRoomIndex);
	   }
   }

   void EventManager::updateEnemies(largeNum sinceLastUpdate)
   {
	     //**Due to an abundance of unforeseen cyclic dependency problems,
		 //  going to try here to establish an attack running logic here that hopefully won't crash...
		 //vector<Enemy*> enemies = GameData::m_map.handleEnemyAttacks();

	     //Text feedback for each enemy update [in case such requires to be displayed / sent to players?
	     //vector<string> enemyFeedback = GameData::m_map.updateEnemies(sinceLastUpdate);
	     vector<Enemy*> /*updateEnemies*/enemies = GameData::m_map.updateEnemies(sinceLastUpdate);

		 vector<Enemy*>::iterator enemyItr = enemies.begin();
		 //vector<Enemy*>::iterator itr = 

		 for(enemyItr = enemies.begin(); enemyItr != enemies.end(); ++enemyItr)
		 {
			 //...?
			 Communication::sendRoom( (*enemyItr)->getName() + " " + (*enemyItr)->NextAction(), (*enemyItr)->Room() );

			 if( (*enemyItr)->IsAttackReady() )
			 {
                if( (*enemyItr)->TargetName() != "" )
				{
			       Player& target = *PlayerDatabase::findfull( (*enemyItr)->TargetName() );
				   if( (*enemyItr)->NextAction().find((*enemyItr)->getSpecName()) != -1 )
				   {
				      short power = 0;
					  short accuracy = 0;
					  if( (*enemyItr)->isSpecMagic() )
					  {
						  power = (*enemyItr)->getSpecPower() * ((*enemyItr)->getIntelligence()/2);
						  accuracy = (*enemyItr)->getSpecAcc() + ((*enemyItr)->getIntelligence()/2);
					  }
					  else
					  {
						  power = (*enemyItr)->getSpecPower() + ((*enemyItr)->getStrength()/2);
						  accuracy = (*enemyItr)->getSpecAcc() + ((*enemyItr)->getDexterity()/2);
					  }

					  target.SendString( (*enemyItr)->specialAttack(target, accuracy, power));
				   }
				   else
				   {
				      target.SendString(red + (*enemyItr)->attack(target) );
				   }
			       if(target.CurrHP() <= 0)
			       {
			          //Communication::sendRoom(target.Name() + " has fallen!", target.Room() );
				      //target.respawn();
				      GameData::m_map.getRoom( (*enemyItr)->Room() ).DontUpdate() = true;
			       }
				}
			 }
		 }
   }
}