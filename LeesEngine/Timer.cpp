#include "Timer.h"

namespace LeesEngine
{
   Timer::Timer(int dummy)
   {
      counter = 0;
	  totalDist = 0;
	  totalRate = 0;
	  initSysTime = 0;
	  subCounter = 0;
	  initSysTime = getTimeElapsed();
	  oldCounter = counter;
   }

   largeNum Timer::getTimeElapsed()
   {
      QueryPerformanceCounter( (LARGE_INTEGER*)&totalDist);
	  QueryPerformanceFrequency( (LARGE_INTEGER*)&totalRate);
	  counter = (totalDist / totalRate * 1000) - initSysTime;
	  sinceLastUpdate = counter - oldCounter;
	  oldCounter = counter;
	  subCounter += sinceLastUpdate;

	  return counter;
   }

   bool Timer::hasTimePassed(short p_time)
   {
      if(subCounter >= p_time)
	  {
	     subCounter = 0;
		 return true;
	  }
	  //else
	  return false;
   }

   largeNum Timer::getSinceLastUpdate()
   {
	   return sinceLastUpdate;
   }
}