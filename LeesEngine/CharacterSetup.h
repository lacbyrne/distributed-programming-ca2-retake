#include "../SocketLib/SocketLib.h"
#include "../SimpleMUD/PlayerDatabase.h"

#include <ctime>

using namespace SocketLib;
using namespace SimpleMUD;

namespace LeesEngine
{
    #ifndef CHARACTER_SETUP_H
    #define CHARACTER_SETUP_H

	//Handler states
	enum States
	{
		RaceChoice,
		StatIncreaser
	};

    class CharacterSetup : public Telnet::handler
	{
		typedef Telnet::handler thandler;

	    public:
		   CharacterSetup( Connection<Telnet>& p_conn, player p_player)
        : thandler( p_conn )
    {
        m_player = p_player;
    }

    // ------------------------------------------------------------------------
    //  Handler Functions
    // ------------------------------------------------------------------------
    void Handle( string p_data );
    void Enter();
    void Leave();
    void Hungup();
    void Flooded();

	private:
		player m_player;
		States m_state;
		void printSetupMenu();
		void setInitialStats(); //Even if the player is allowed to customise stats, still need to start them on a higher base than 0 or 1 to guide player
		void initStat(short& stat);
	};

    #endif
}