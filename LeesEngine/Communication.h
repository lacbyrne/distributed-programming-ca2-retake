//For propogating information around multiple players, or handling global data
#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include <string>

#include "../BasicLib/BasicLib.h"
#include "../SimpleMUD/PlayerDatabase.h"

using std::string;
using namespace BasicLib;
using namespace SimpleMUD;

using SocketLib::green;
using SocketLib::yellow;
using SocketLib::reset;

class Communication
{
    public:

		static void sendRoom(string message, short roomIndex)
		{
			operate_on_if( PlayerDatabase::begin(),
                           PlayerDatabase::end(),
				           playersend( message ),
                           playerinroom( roomIndex ) );
		}

		//...?
		static bool findAnActivePlayer()
		 {
			 PlayerDatabase::iterator itr;
			 for(itr = PlayerDatabase::begin(); itr != PlayerDatabase::end(); ++itr)
			 {
				 if(itr->isActive())
				 {
					 return true;
				 }
			 }

			 return false;
		 }

	   static bool playerInRoom(short room)
		 {
			 PlayerDatabase::iterator itr;
			 for(itr = PlayerDatabase::begin(); itr != PlayerDatabase::end(); ++itr)
			 {
				 if(itr->isActive() && itr->Room() == room)
				 {
					 return true;
				 }
			 }

			 return false;
		 }

	   static void showScores(Player* player) //Does this go here...??
	   {
		   //Find who has the highest loot
		   Player* p = 0;
		   int maxLoot = 0;
		   PlayerDatabase::iterator itr;
		   for(itr = PlayerDatabase::begin(); itr != PlayerDatabase::end(); ++itr)
		   {
			   if(itr->getLoot() >= maxLoot)
			   {
				   p = &(*itr);
				   maxLoot = p->getLoot();
			   }
		   }

		   string output = "";

		   //Now display loot for all players, highlighting the chap who has the highest
		   //[[Of course, ordering the list by loot would be preferrable... if not so difficult to do =| ]]
		   player->SendString("=========== CURRENT SCORES ===================\r\n");
		   for(itr = PlayerDatabase::begin(); itr != PlayerDatabase::end(); ++itr)
		   {
		      if( &(*itr) == p)
			  {
			     output += green + "***";  
			  }
			  else
			  {
			     output += yellow;
			  }

			  output += itr->Name() + "\t\tLoot: " + BasicLib::tostring(itr->getLoot());

			  if( &(*itr) == p)
			  {
			     output += "***";  
			  }

			  output += "\r\n";
		   }

		   player->SendString(output);
		   player->SendString("=======================================\r\n");

		   if(player == p)
		   {
			   player->SendString(green + " You're sailin' on the top of the leaderboard! :) ");
		   }
		   else
		   {
			   int lootNeeded = (p->getLoot() - player->getLoot() ) + 1;
			   player->SendString("... You still need at least " + BasicLib::tostring(lootNeeded) + " more loot to top the leaderboard");
		   }
	   }

	   static string getWhoList()
	   {
	      string toReturn = "===============LIST OF PLAYERS ===================\r\n";

		  PlayerDatabase::iterator itr;
		  for(itr = PlayerDatabase::begin(); itr != PlayerDatabase::end(); ++itr)
		   {
			  if(itr->isActive())
			  {
			     toReturn += green;
			  }

			  toReturn += itr->getName();
			  toReturn += "     [CURRENTLY";

			  if(itr->isActive() )
			  {
			     toReturn += " ONLINE]\t\t";
			  }
			  else
			  {
				 toReturn += " OFFLINE]\t\t";
			  }
			  
			  toReturn += " RANK: " + itr->Rank() + reset + "\r\n";
		   }

		   toReturn += "================================================\r\n";

		   return toReturn;
	   }
};

#endif