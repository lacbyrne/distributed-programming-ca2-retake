#include "Messages.h"

namespace LeesEngine
{
	const string Messages::Login = "Logging into the game...";
	const string Messages::WrongPassword = "ERROR: Player exists but wrong password was provided";
	const string Messages::AlreadyLoggedin = "ERROR: That player is already logged in!";
	const string Messages::NewPlayer = "Welcome to the game! Creating a new player...";
	const string Messages::IllegalName = "ERROR: That name/password cannot be accepted by the system.";
}