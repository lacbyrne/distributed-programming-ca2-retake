#include "Item.h"

namespace LeesEngine
{
	Item::Item()
	{
		m_name = "";
		m_description = "It's an item";
		m_sellValue = 0;
		m_use = "";
		m_type = ItemType::WieldableItemType;
	}

	Item::Item(string name)
	{
		m_name = name;
	}

	Item::Item(string p_name, ItemType p_type, string p_description, int p_sellValue, string p_use)
	{
		m_name = p_name;
		m_description = p_description;
		m_sellValue = p_sellValue;
		m_use = p_use;
		m_type = p_type;
	}
}