#include "DataStore.h"

namespace LeesEngine
{
    //Need to tell the cpp about any static attributes, otherwise compiler will complain that any that it attempts to use are 'unresolved' =P
	//vector<Item> DataStore::m_itemTypes(50);
	//map<string, Item*> DataStore::m_itemTypes;
	map<string, Item*> DataStore::m_itemTypes;
	vector<string> DataStore::m_itemNames;
    vector<vector<Enemy>> DataStore::m_enemyTypes;
	//For testing (though *might* need it later anyway)
	vector<string> DataStore::m_enemyNames;
	//vector<PlayerData> DataStore::m_playersLogins;
	//vector<Player> DataStore::m_activePlayers;

	void DataStore::setupData()
	{
		//Probably need to 'prepare' the multivector before using it...
		m_enemyTypes.push_back( vector<Enemy>() );
		m_enemyTypes.push_back( vector<Enemy>() );
		m_enemyTypes.push_back( vector<Enemy>() );

		//loadPlayerLogins();
		//Set up the map, too!
		loadItems();
		loadEnemies();
	}

	//Load up data for creating (instances of) items
	void DataStore::loadItems()
	{
		ifstream in("items/items.txt");

		if(in)
		{
			string data;
			string header;
			short offset = 0;

			while(in.good())
			{
				getline(in, data, '/');

				if(data.find('<') == data.rfind('<') )
				{
					break;
				}
				else
				{
				   //NOTE: C++ substring takes end char to be LENGTH -- not end position!! Silly C++... {*sigh*}
				   offset = data.find('<');
				   data = data.substr(offset, /*data.rfind('\n')*/data.length()-2-offset);
				   //string header = data.substr(0, data.find('\n'));
				   header = data.substr(data.find('<')+1, data.find('>')-1);
				   data = data.substr(data.find('\n')+1, data.length() );
				}

				//Now do stuff with this data!

				//Common to all items: name, description, sell value, use?
				offset = data.find(':')+2;
				string name = data.substr(offset, data.find('\n') - offset );
				data = data.substr(data.find('\n')+1, data.length() );

				//m_itemNames.push_back(name); //PROBLEM : keys are case sensitive
				name = BasicLib::LowerCase(name);
				m_itemNames.push_back( name );

				offset = data.find(':')+2;
				string description = data.substr(offset, data.find('\n') - offset );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				int sellValue = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				string use = data.substr(offset, data.find('\n') - offset );
				data = data.substr(data.find('\n')+1, data.length() );

				if(header == "I") //item
				{
					Item* toAdd = new Item(name, ItemType::BasicItemType, description, sellValue, use);
					m_itemTypes[name] = toAdd;
				}
				else if(header == "W") //wieldable
				{
				   offset = data.find(':')+2;
				   short minPower = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   short maxPower = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

   				   offset = data.find(':')+2;
				   short accuracy = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   short swingTime = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   bool twoHanded = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   short dv = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   short pv = atoi(data.substr(offset, data.length()).c_str() );

				   WieldableItem* toAdd 
				   = new WieldableItem(name, ItemType::WieldableItemType, description, sellValue, use, 
				                       minPower, maxPower, accuracy, swingTime, twoHanded, dv, pv, false);
				   m_itemTypes[name] = toAdd;
				}
				else if(header == "A") //armour
				{
				   offset = data.find(':')+2;
				   short dv = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   short pv = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   int weight = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   string equippedOn = data.substr(offset, data.length() );

				   Armour* toAdd = new Armour(name, ItemType::ArmourType, description, sellValue, use, dv, pv, weight, false, equippedOn);
				   m_itemTypes[name] = toAdd;
				}
				else if(header == "MI") //magic item
				{
				   offset = data.find(':')+2;
				   short effect = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   short charges = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   short range = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   int rechargeTime = atoi(data.substr(offset, data.length() ).c_str() );

				   MagicItem* toAdd = new MagicItem(name, ItemType::MagicItemType, description, sellValue, use, effect, charges, range, rechargeTime);
				   m_itemTypes[name] = toAdd;
				}
				else if(header == "ME") //magic equipment
				{
				   //Might be a bit trickier...
				   vector<string> stats;
				   vector<short> modifiers;
				   string parsedAtom;

				   offset = data.find(':')+2;
				   string multivalLine = data.substr(offset, data.find('\n') - offset ).c_str();
				   while(multivalLine.find(',') != -1)
				   {
					   parsedAtom = multivalLine.substr(0, multivalLine.find(',') );
					   stats.push_back(parsedAtom);

					   multivalLine = multivalLine.substr(multivalLine.find(',')+2, multivalLine.length());
				   }
				   //Above logic leaves the last item in the string remaining to be found
				   stats.push_back(multivalLine);

				   data = data.substr(data.find('\n')+1, data.length() );

				   offset = data.find(':')+2;
				   multivalLine = data.substr(offset, data.find('\n') - offset ).c_str();
				   while(multivalLine.find(',') != -1)
				   {
					   parsedAtom = multivalLine.substr(0, multivalLine.find(',') );
					   modifiers.push_back( atoi(parsedAtom.c_str() ) );

					   multivalLine = multivalLine.substr(multivalLine.find(',')+2, multivalLine.length());
				   }
				   //Above logic leaves the last item in the string remaining to be found
				   modifiers.push_back( atoi(multivalLine.c_str() ) );

				   MagicEquipment* toAdd = new MagicEquipment(name, ItemType::MagicEquipmentType, description, sellValue, use, stats, modifiers, false);
				   m_itemTypes[name] = toAdd;
				}
			}
		}
	}

	void DataStore::loadEnemies()
	{
		ifstream in("enemies/enemies.txt");

		if(in)
		{
			string data;
			string header;
			short offset = 0;

			while(in.good())
			{
				getline(in, data, '}');

				if(data == "")
				{
					break;
				}
				else
				{
				   //NOTE: C++ substring takes end char to be LENGTH -- not end position!! Silly C++... {*sigh*}
				   offset = data.find('{')+2;
				   data = data.substr(offset, /*data.rfind('\n')*/data.length()-1-offset);
				}

				//Now do stuff with this data!

				offset = data.find(':')+2;
				string name = data.substr(offset, data.find('\n') - offset );
				data = data.substr(data.find('\n')+1, data.length() );

				//m_itemNames.push_back(name); //PROBLEM : keys are case sensitive
				name = BasicLib::LowerCase(name);
				m_enemyNames.push_back( name );

				offset = data.find(':')+2;
				short maxHP = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short strength = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short endurance = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short dexterity = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short agility = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short intelligence = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short spirit = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short xpYield = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short aggression = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				string specName = data.substr(offset, data.find('\n') - offset );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short specPower = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short specAcc = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short specChance = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				int specRecharge = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				char specMagicChar = data.substr(offset, data.find('\n') - offset)[0];
				bool specMagic;

				if(specMagicChar == 'y')
				{
				   specMagic = true;
				}
				else
				{
				   specMagic = false;
				}

				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				string description = data.substr(offset, data.find('\n') - offset );
				data = data.substr(data.find('\n')+1, data.length() );

				offset = data.find(':')+2;
				short dangerLevel = atoi(data.substr(offset, data.find('\n') - offset ).c_str() );
				data = data.substr(data.find('\n')+1, data.length() );

				//Build an enemy!
				Attributes attributes = {strength, endurance, dexterity, agility, intelligence, spirit, 0};
				Enemy e(name, maxHP, attributes, xpYield, aggression, specName, specPower, specAcc, specChance, specRecharge, specMagic, description);
				//m_enemyTypes.push_back(e);
				m_enemyTypes[dangerLevel-1].push_back(e);

			}
		}
	}

	Item* DataStore::getRandomItem(bool canBeLoot)
	{
		short randIndex = rand() % (m_itemNames.size()+1);
		if(randIndex >= m_itemNames.size() )
		{
			if(canBeLoot)
			{
			   return new Item("loot");
			}
			else
			{
				randIndex = m_itemNames.size()-1;
			}
		}

		string randKey = m_itemNames[randIndex];
		return m_itemTypes[randKey];
	}

	Item* DataStore::getSpecificItem(string name)
	{
		Item* toReturn = m_itemTypes[name];
		//return m_itemTypes[toReturn];
		return toReturn;
	}

	Enemy DataStore::createEnemy(short dangerLevel)
	{
       //WIP =P

	   if(dangerLevel-1 >= m_enemyTypes.size() )
	   {
		   dangerLevel = m_enemyTypes.size()-1;
	   }

	   srand( time(NULL) );
	   short index = rand() % m_enemyTypes[dangerLevel-1].size();
	   return m_enemyTypes[dangerLevel-1][index];
	}

	Enemy DataStore::getSpecificEnemy(string name)
	{
	    //Might not be able to use m_enemyNames here after all... =|
		for(short i = 0; i < m_enemyTypes.size(); i++)
		{
	       for(short j = 0; j < m_enemyTypes[i].size(); j++)
		   {
			   if(m_enemyTypes[i][j].getName() == name)
			   {
				   return m_enemyTypes[i][j];
			   }
		   }
		}

		Enemy e;
		return e;
	}
}