#include "Enemy.h"

#include <iostream>

namespace LeesEngine
{
	Enemy::Enemy() : Actor()
	{
		m_xpYield = 0;
		m_aggression = 0;
		m_ally = "";
		m_specName = "";
		m_specPower = 0;
		m_specAcc = 0;
		m_specChance = 0;
		m_specRecharge = 0;
		m_description = ""; 
		m_targetName = "";
		m_nextActionTime = 3000;
	}

	Enemy::Enemy(string p_name) : Actor()
	{
		m_name = p_name;
	}

	Enemy::Enemy(string p_name, short p_maxHP, short p_currHP, short p_roomIndex, short p_x, short p_y, Attributes p_attributes,
				  int p_xpYield, short p_aggression, string p_ally, string p_specName, short p_specPower, short p_specAcc, short p_specChance,
				  int p_specRecharge, bool p_specMagic, string p_description)
				  : Actor(p_name, p_maxHP, p_currHP,/*, p_attributes*/ p_roomIndex, p_x, p_y)
	{

	}

	Enemy::Enemy(string p_name, short p_maxHP, Attributes p_attributes, int p_xpYield, short p_aggression,
				  string p_specName, short p_specPower, short p_specAcc, short p_specChance, int p_specRecharge, bool p_specMagic, string p_description)
				  : Actor(p_name, p_maxHP, p_maxHP, 0, 0, 0)
	{
		m_name = p_name;
		m_attributes = p_attributes;
		m_xpYield = p_xpYield;
		m_aggression = p_aggression;
	    m_ally = "";
		m_specName = p_specName;
	    m_specPower = p_specPower;
        m_specAcc = p_specAcc;
		m_specChance = p_specChance;
		m_specRecharge = p_specRecharge;
		m_specMagic = p_specMagic;
	    m_description = p_description;
		m_timePassed = 0;
		m_targetName = "";
		m_nextActionTime = 3000;
	}

	string Enemy::getName()
	{
		return m_name;
	}

	short Enemy::getAttackAccuracy()
	{
		return m_attributes.dexterity;
	}

	short Enemy::getEvasion()
	{
		return m_attributes.agility; //Anything else? =P
	}

	short Enemy::getAttackPower()
	{
		srand( time(NULL) );
		return m_attributes.strength + ( rand() % m_attributes.strength ) + ( rand() % m_attributes.dexterity)/2;
	}

	short Enemy::getDefence()
	{
		return (short)m_attributes.endurance/1.5f;
	}

	short Enemy::checkKO()
	{
		if(m_currHP < 1)
		{
			return m_xpYield;
		}
		else
		{
	       return 0;
		}
	}

	void Enemy::move(short p_direction)
	{
		
	}

	int Enemy::getAttackTime()
	{
	   return 0;
	}
 
	bool Enemy::isAlly(string p_name)
	{
		return false;
	}

	void Enemy::update(largeNum sinceLastUpdate)
	{
		m_timePassed += sinceLastUpdate;

		//if(m_timePassed > 5000) //Temporary... @_@
		if(m_timePassed > m_nextActionTime)
		{
			m_timePassed = 0;

			//srand( time(NULL) );
			short attackRoll = rand() % 10;
			short specRoll = rand() % 20;

			if(attackRoll < m_aggression+2 )
			{
			   m_attackReady = true;

			   if(specRoll < m_specChance)
			   {
			      m_nextAction = m_specName;
				  m_nextActionTime = m_specRecharge;
			   }
			   else
			   {
			      //return m_name + " attacks ";
			      m_nextAction = "attacks ";
				  m_nextActionTime = (20000 / m_attributes.agility );
			   }
			}
			else
			{
			   m_attackReady = false;
			   //return m_name + " scratches itself ";
			   m_nextAction = "looks around";
			   m_nextActionTime = 2500;
			}
		}
		else
		{
			m_attackReady = false;
			m_nextAction = "";
		}

		//return "";
	}

	bool Enemy::isEnemy()
	{
		return true;
	}
}