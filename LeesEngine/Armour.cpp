#include "Armour.h"

namespace LeesEngine
{
    Armour::Armour(string p_name, ItemType p_type, string p_description, int p_sellValue, string p_use,
						short p_DV, short p_PV, short p_weight, bool p_equipped, string p_equippedOn)
	: Item(p_name, p_type, p_description, p_sellValue, p_use)
	{
		m_DV = p_DV;
		m_PV = p_PV;
		m_weight = p_weight;
		m_equipped = p_equipped;
		m_equippedOn = p_equippedOn;
	}
}