#include "GameMap.h"
#include "Communication.h"

namespace LeesEngine
{
	GameMap::GameMap(/*Array<Room> p_rooms*/ bool dummy) /*: m_rooms(p_rooms)*/
	{
		
	}

	Room& GameMap::getRoom(int index)
	{
	    return m_rooms[index];
	}

	/*void GameMap::setRoom(Room& data, int index)
	{
		m_rooms[index] = data;
	}*/

	void GameMap::setup()
	{
		//Room 1
		Exits exits1 = {1,-1,-1,-1};
		Room room1(0, "First room", "regular", 1, exits1, "First room");
		m_rooms[0] = room1;

		Exits exits2 = {2,0,3,4};
		Room room2(1, "Second room", "regular", 1, exits2, "Second room. It's very spacious.");
		m_rooms[1] = room2;

		Exits exits3 = {5,1,6,7};
		Room room3(2, "Third room", "regular", 2, exits3, "Third room. The surrounding room has a bit of an aura of danger, watch out.");
		m_rooms[2] = room3;

		Exits exits4 = {-1,-1,-1,1};
		Room room4(3, "Fourth room", "regular", 3, exits4, "Fourth room. It appears to be a cul-de-sac. You fear that quite dangerous foes might pop up here.");
		m_rooms[3] = room4;

		Exits exits5 = {-1,-1,1,-1};
		Room room5(4, "Fifth room, shop", "shop", 1, exits5, "Ooh, a shop! You can buy and sell things here (in case you didn't know what a shop was");
		m_rooms[4] = room5;

		Exits exits6 = {-1,2,-1,-1};
		string description6 = "It's the end of the line...for now.";
		description6 += " You feel the aura of particularly dangerous monsters... is their master close by??";
		Room room6(5, "Sixth room, danger zone", "regular", 5, exits6, description6);
		m_rooms[5] = room6;

		Exits exits7 = {-1,-1,-1,2};
		Room room7(6, "Seventh room, puzzle area", "special", 1, exits7, "This seems to be a special room... with something or other to solve?");
		m_rooms[6] = room7;

		Exits exits8 = {-1,-1,2,-1};
		Room room8(7, "Eight room", "regular", 4, exits8, "Another cul-de-sac. I wonder what sorts of dangers abound here?");
		m_rooms[7] = room8;
	}

	void GameMap::saveMap()
	{
		//Save each room?
		for(short i = 0; i < NUM_ROOMS; i++)
		{
			m_rooms[i].save();
		}
	}

	void GameMap::loadMap()
	{
		for(short i = 0; i < NUM_ROOMS; i++)
		{
			m_rooms[i].load(i);
		}
	}

	//Kind of only temporary anyway, until 2D movement gets implemented (if it does) =P
	string GameMap::showRoomExits(short roomIndex)
	{
		string output;
		Room& room = m_rooms[roomIndex]; //Makes the code a little neater

		if(room.getExits().north > -1)
		{
			output += " north to " + m_rooms[room.getExits().north].RoomName() + "\r\n";
		}
		if(room.getExits().south > -1)
		{
			output += " south to " + m_rooms[room.getExits().south].RoomName() + "\r\n";
		}
		if(room.getExits().west > -1)
		{
			output += " west to " + m_rooms[room.getExits().west].RoomName() + "\r\n";
		}
		if(room.getExits().east > -1)
		{
			output += " east to " + m_rooms[room.getExits().east].RoomName() + "\r\n";
		}

		return output;
	}

	/*Enemy& GameMap::spawnEnemy()
	{
		
	}*/

	short GameMap::designateSpawnRoom()
	{
		//Should select a room at random, with particular weighting towards a room where at least one player is in...
		//For now, spawn 'em in second room!
		//short dangerLevel = m_rooms[0].DangerLevel();
		//Try a room that has at least one player... @_@
		//Also: enemies don't spawn in special areas
		short i = 0;
		for(i = 1; i < NUM_ROOMS; i++)
		{
			if( m_rooms[i].getPlayers().size() > 0 && m_rooms[i].getType() == "regular")
		    {
			    return i;
		    }
		}
		
		return -1;
	}

	/*vector<string>*/vector<Enemy*> GameMap::updateEnemies(largeNum timePassed)
	{
		//vector<string> toReturn;
		vector<Enemy*> toReturn;
		vector<Enemy*> roomEnemies;
		vector<Enemy*>::iterator itr;
		
		int i = 0;
		string roomFeedback;
		for(i = 0; i < NUM_ROOMS; i++)
		{
			if(Communication::playerInRoom(i) )
			{
			   roomEnemies = m_rooms[i].updateEnemies(timePassed);
			   itr = roomEnemies.begin();
			   for(itr = roomEnemies.begin(); itr != roomEnemies.end(); ++itr)
			   {
 			      toReturn.push_back(*itr);
			   }
		    }
		 }

		return toReturn;
	}

	vector<Enemy*> GameMap::handleEnemyAttacks()
	{
		vector<Enemy*> toReturn;
		int i = 0;
		for(i = 0; i < NUM_ROOMS; i++)
		{
			vector<Enemy*> fromRoom = m_rooms[i].getAttackingEnemies();

			vector<Enemy*>::iterator itr = fromRoom.begin();
		    for(itr = fromRoom.begin(); itr != fromRoom.end(); ++itr)
		    {
		       toReturn.push_back(*itr);
		    }
		}

		return toReturn;
	}

	void GameMap::updateRooms(largeNum sinceLastUpdate)
	{
       short i = 0;
	   for(i = 0; i < NUM_ROOMS; i++)
	   {
		   if(m_rooms[i].getType() != "regular")
		   {
		      m_rooms[i].update(sinceLastUpdate);
		   }
	   }
	}
}