/**

'Player' -- models characteristics of the player character and what it can do in the game world

**/

/*#ifndef PLAYER_H
#define PLAYER_H

#include "../SocketLib/SocketLib.h"
#include "../BasicLib/BasicLib.h"

#include "../SimpleMUD/DatabasePointer.h"

#include "../LeesEngine/Actor.h"
//Why does Item need an include rather than just a simple declaration?
//When compiling m_inventory, the compiler will complain that it cannot construct a vector of Items
//because it does not know how much memory space each Item takes!
#include "../LeesEngine/Item.h"
#include "../LeesEngine/Enemy.h" //?

#include <math.h>
#include <string>
#include <vector>

using SocketLib::Connection;
using SocketLib::Telnet;
using namespace BasicLib;
using std::ostream;
using std::istream;
using std::string;

//using namespace SimpleMUD;
using namespace LeesEngine;

//namespace LeesEngine
namespace SimpleMUD
{

class Player : public Entity, Actor
{
public:

	Player();
	Player(string p_name, ActorAttributes p_attributes); //Default-ish constructor
		   Player(string p_name, short p_HP, ActorAttributes p_attributes, short p_roomIndex, short p_x, short p_y,
			      string p_race, int p_loot, int p_xp, vector<Item> p_inventory, vector<string> p_allies, string p_rank);
		   void move(short p_direction);
		   int getAttackTime();
		   short getAttackAccuracy();
		   short getAttackPower();
		   short getEvasion();
		   short getDefence();
		   short checkKO();
		   bool isAlly(string p_name);
		   short getDV();
		   short getPV();
		   short getBlockChance(int p_attackTime);
		   short getRoom(); //Why is this only in Player, and not in Actor?
		   short acquireItem(Item item); //Changed from 'getItem' because this is too easy to confuse with accessors
		   void setEquipped(string itemName);
		   string showInventory();
		   string showStats();
		   void levelUp(); //I think this might have been put in the UML as returning a short by mistake...?
		   string offerPeace(Enemy& e);
		   string collaborateWith(Player& target);
		   string giveTo(Item item, Player& receiver);
		   string drop(Item item);
		   string getPlayerSaveData();
		   string getRank();
		   string getLastQueryAsked();
		   void setLastQueryAsked(string query);
		   //Test
		   short getCharisma();
    inline Connection<Telnet>*& Conn()      { return m_connection; } //From SimpleMUD
    void SendString( const string& p_string ); //From SimpleMUD
	inline string& Password()               { return m_pass; }
	inline bool& LoggedIn()                 { return m_loggedin; }
    inline bool& Active()                   { return m_active; }
    inline bool& Newbie()                   { return m_newbie; }
	// ------------------------------------------------------------------------
    //  Stream Functions
    // ------------------------------------------------------------------------
    friend ostream& operator<<( ostream& p_stream, const Player& p );
    friend istream& operator>>( istream& p_stream, Player& p );

private:

	string m_race;
		   int m_loot;
		   int m_xp;
		   short m_level;
		   vector<Item> m_inventory;
		   vector<string> m_allies;
		   string m_rank;
		   string m_lastQueryAsked;
    Connection<Telnet>* m_connection;
	string m_pass;
	bool m_loggedin;
    bool m_active;
    bool m_newbie;
	

};  // end class Player

// --------------------------------------------------------------------
//  functor that determines if a player is active
// --------------------------------------------------------------------
struct playeractive
{ 
    inline bool operator() ( Player& p_player )
    {
        return p_player.Active();
    }

    inline bool operator() ( Player* p_player )
    {
        return p_player != 0 && p_player->Active();
    }
};

// --------------------------------------------------------------------
//  functor that determines if a player is logged in
// --------------------------------------------------------------------
struct playerloggedin
{ 
    inline bool operator() ( Player& p_player )
    {
        return p_player.LoggedIn();
    }

    inline bool operator() ( Player* p_player )
    {
        return p_player != 0 && p_player->LoggedIn();
    }

};

// --------------------------------------------------------------------
//  functor that sends a string to a player.
// --------------------------------------------------------------------
struct playersend
{
    const string& m_msg;

    playersend( const string& p_msg )
        : m_msg( p_msg ) { }

    void operator() ( Player& p )
    {
        p.SendString( m_msg );
    }

    void operator() ( Player* p )
    {
        if( p != 0 )
            p->SendString( m_msg );
    }
};

}   // end namespace


#endif*/