#include "Room.h"

#include "GlobalData.h"
//#include "GameData.h"
#include "DataStore.h"

using namespace std;

namespace LeesEngine
{
	Room::Room() /*: m_players(25, 5) , m_enemies(  t25, 5), m_items(25, 5)*/ : m_enemies(), m_players(), m_items()
	{
		m_roomIndex = 0;
		m_roomName = "";
		m_roomType = "";
		m_dangerLevel = 1;
		//m_exits = {1, 1, 1, 1};
		m_exits.north = -1;
		m_exits.south = -1;
		m_exits.west = -1;
		m_exits.east = -1;
		m_description = "Wot?";
		
		m_dontUpdate = false;
	}

	Room::Room(short p_roomIndex, string p_roomName, string p_roomType, short p_dangerLevel, 
			    /*Array<string> p_players, Array<Enemy> p_enemies, Array<Item> p_items,*/
				Exits p_exits, string p_description) : m_enemies()
				/*: m_players(p_players) , m_enemies(p_enemies), m_items(p_items)*/
	{
		m_roomIndex = p_roomIndex;
		m_roomName = p_roomName;
		m_roomType = p_roomType;
		m_dangerLevel = p_dangerLevel;
		m_exits = p_exits;
		m_description = p_description;

		m_dontUpdate = false;
	}

	/*Room::Room(short p_roomIndex, string p_roomType, short p_dangerLevel,  Exits p_exits, string p_description)
				: m_players(25, 5) , m_enemies(25, 5), m_items(25, 5)
	{
		m_roomIndex = p_roomIndex;
		m_roomType = p_roomType;
		m_dangerLevel = p_dangerLevel;
		m_exits = p_exits;
		m_description = p_description;
	}*/

	Room::Room(string p_description)
		        /*: m_players(25, 5) , m_enemies(25, 5), m_items(25, 5)*/
	{
		m_roomIndex = 0;
		m_roomType = "";
		m_dangerLevel = 1;
		//m_exits = {1, 1, 1, 1};
		m_exits.north = -1;
		m_exits.south = -1;
		m_exits.west = -1;
		m_exits.east = -1;
		m_description = p_description;

		m_dontUpdate = false;
	}

	string Room::getDescription()
	{
		return m_description;
	}

	Exits Room::getExits()
	{
		return m_exits;
	}

	/*Item Room::getItem(int index)
	{
		return m_items[index];
	}*/

	void Room::save()
	{
		//This is a tricky one... since it will involve
		//[]Loading all the items in the room [and, optionally, their 2D positions]
		//[]Loading all the monsters in the room
		//...on top of danger level and [optionally] room description.
		//Hmmm. =|

		string fileSaveName = "rooms//room" + BasicLib::tostring(m_roomIndex+1);
		fileSaveName += ".txt";

		ofstream out( fileSaveName.c_str() );

        //Write, write, write!
		out << "<ROOM INDEX>" << m_roomIndex << "\n";
		out << "<ROOM NAME>" << m_roomName << "\n";
		out << "<DANGER LEVEL>" << m_dangerLevel << "\n";
		out << "<EXITS>" << m_exits.north << "," << m_exits.south << "," << m_exits.west << "," << m_exits.east << "\n";
		out << "<DESCRIPTION>" << m_description << "\n";
		out << "<ITEMS>";
		//Write out each item... when function for doing so is implemented
		//Items that have not yet been used should be vanilla anyway, so just need to save their names?
		string items = "";

		//size_t i = 0;
		/*for(i = 0; i < m_items.size(); i++)
		{
			if(items != "")
    		{ 
               items += ", ";
		    }
		    items += m_items[i]->Name();
		}*/
		vector<Item*>::iterator itr = m_items.begin();
		for(itr = m_items.begin(); itr != m_items.end(); ++itr)
		{
			if(items != "")
    		{ 
               items += ", ";
		    }
		    items += (*itr)->Name();
		}

		out << items << "\n";

		out << "<ENEMIES>";
		string enemies = "";
		//Write out each enemy
		// [[NEED TO DETERMINE VOLATILE DATA AND STORE IT!!]]
		//  \-->HP only...?
		vector<Enemy>::iterator enemyItr = m_enemies.begin();
		for(enemyItr = m_enemies.begin(); enemyItr != m_enemies.end(); ++enemyItr)
		{
			if(enemies != "")
    		{ 
               enemies += ", ";
		    }
			enemies += enemyItr->getName();
			enemies += " " + BasicLib::tostring(enemyItr->CurrHP());
		}

		out << enemies << "\n";
		out << "<TYPE>" << m_roomType;


		out.flush();
	}

	void Room::load( short roomIndex )
	{
		string fileLoadName = "rooms/room" + BasicLib::tostring( (roomIndex+1) ); 
		fileLoadName += ".txt";

		ifstream in( fileLoadName.c_str() );

		if(in)
		{
	       vector<string> collectedData;
	       string data;

	       while(in.good() )
	       {
		        getline(in, data);
		        //Name
		        collectedData.push_back(data.substr(data.find('>')+1, data.length()));
	       }

	       vector<string>::iterator iter = collectedData.begin() /*+ 1*/;
		   m_roomIndex = atoi(iter->c_str());
		   ++iter;
		   m_roomName = *iter;
		   ++iter;
	       m_dangerLevel = atoi(iter->c_str());
		   ++iter;
		   //Exits
		   string exits = *iter;
		   m_exits.north = atoi(exits.substr(0, exits.find(',')).c_str());
		   exits = exits.substr(exits.find(',')+1, exits.length());
		   m_exits.south = atoi(exits.substr(0, exits.find(',')).c_str());
		   exits = exits.substr(exits.find(',')+1, exits.length());
		   m_exits.west = atoi(exits.substr(0, exits.find(',')).c_str());
		   exits = exits.substr(exits.find(',')+1, exits.length());
		   m_exits.east = atoi(exits.c_str());
		   ++iter;
		   m_description = *iter;
		   ++iter;
		   //Items
		   string items = *iter;
		   string itemName;
		   while(items.find(',') != -1)
	       {
		      itemName = items.substr(0, items.find(',') ) ;
			  cout << itemName << endl;
			  m_items.push_back( DataStore::getSpecificItem( itemName ) );
		      items = items.substr(items.find(',')+2, items.length());
	       }
	       //Above logic leaves the last item in the string remaining to be found
	       if(items != "")
	       {
	          itemName = items;
			  m_items.push_back( DataStore::getSpecificItem( itemName ) );
	       }

		   ++iter;
		   //Enemies
		   //[[Note that enemies in a room must be saved with volatile data, which is appended to the assigned name
		   string enemies = *iter;
		   string enemyName;
		   string enemyHP;
		   short lastSpace = 0;
		   while(enemies.find(',') != -1)
	       {
		      enemyName = enemies.substr(0, enemies.find(',') ) ;
			  lastSpace = enemyName.rfind(' ');
			  enemyHP = enemyName.substr(lastSpace+1, enemyName.length() );
			  cout << enemyHP << endl;
			  enemyName = enemyName.substr(0, lastSpace);
			  Enemy e = DataStore::getSpecificEnemy(enemyName);
			  e.CurrHP() = atoi(enemyHP.c_str() );
			  e.Room() = m_roomIndex;
			  m_enemies.push_back( e );
		      enemies = enemies.substr(enemies.find(',')+2, enemies.length());
	       }
	       //Above logic leaves the last item in the string remaining to be found
	       if(enemies != "")
	       {
	          enemyName = enemies;
			  //m_items.push_back( DataStore::getSpecificItem( itemName ) );
			  lastSpace = enemyName.rfind(' ');
			  enemyHP = enemyName.substr(lastSpace+1, enemyName.length() );
			  enemyName = enemyName.substr(0, lastSpace);
			  cout << enemyHP << endl;
			  Enemy e = DataStore::getSpecificEnemy(enemyName);
			  e.CurrHP() = atoi(enemyHP.c_str() );
			  e.Room() = m_roomIndex;
			  m_enemies.push_back( e );
	       }

		   ++iter;
		   //Type
		   m_roomType = BasicLib::LowerCase(*iter);
		}
	}

	void Room::addItem(Item* item)
	{
       m_items.push_back(item);
	}

	Enemy* Room::addEnemy(Enemy e)
	{
		//**Don't forget to tell the enemy it's in this room!**
		e.Room() = m_roomIndex;
		m_enemies.push_back(e);

		//return *(m_enemies.end()-1);
		return &m_enemies.back();
	}

	string Room::showInhabitants()
	{
		string toReturn = "Players: ";
		
		vector<string>::iterator playerItr = m_players.begin();
		for(playerItr = m_players.begin(); playerItr != m_players.end(); ++playerItr)
		{
	       toReturn += *playerItr;
		   if(playerItr != m_players.end()-1)
		   {
			   toReturn += ", ";
		   }
		}

		toReturn += "\r\nEnemies: ";

		vector<Enemy>::iterator itr = m_enemies.begin();
		for(itr = m_enemies.begin(); itr != m_enemies.end(); ++itr)
		{
	       toReturn += itr->getName();
		   if(itr != m_enemies.end()-1)
		   {
			   toReturn += ", ";
		   }
		}

		toReturn += "\r\nItems: ";

		vector<Item*>::iterator itemItr = m_items.begin();
		for(itemItr = m_items.begin(); itemItr != m_items.end(); ++itemItr)
		{
	       toReturn += (*itemItr)->Name();
		   if(itemItr != m_items.end()-1)
		   {
			   toReturn += ", ";
		   }
		}

		return toReturn;
	}

	string Room::getRoomInfo()
	{
	   string toReturn = "";
	   toReturn += m_description + "\r\nPlayers:";
		
		vector<string>::iterator playerItr = m_players.begin();
		for(playerItr = m_players.begin(); playerItr != m_players.end(); ++playerItr)
		{
	       toReturn += *playerItr;
		   if(playerItr != m_players.end()-1)
		   {
			   toReturn += ", ";
		   }
		}

		toReturn += "\r\nEnemies: ";

		vector<Enemy>::iterator itr = m_enemies.begin();
		for(itr = m_enemies.begin(); itr != m_enemies.end(); ++itr)
		{
	       toReturn += itr->getName();
		   if(itr != m_enemies.end()-1)
		   {
			   toReturn += ", ";
		   }
		}

		if(m_roomType != "shop")
		{
		   toReturn += "\r\nItems: ";

		   vector<Item*>::iterator itemItr = m_items.begin();
		   for(itemItr = m_items.begin(); itemItr != m_items.end(); ++itemItr)
		   {
	          toReturn += (*itemItr)->Name();
		      if(itemItr != m_items.end()-1)
		      {
			     toReturn += ", ";
		      }
		   }
		}

		return toReturn;
	}

	vector<Enemy*> Room::updateEnemies(largeNum timePassed)
	{
		//string toReturn;
		//string enemyFeedback;
		vector<Enemy*> toReturn;
		/*vector<Enemy>::iterator itr = m_enemies.begin();
		for(itr = m_enemies.begin(); itr != m_enemies.end(); ++itr)*/
		size_t i = 0;
		for(i = 0; i < m_enemies.size(); i++ )
		{
			//enemyFeedback = itr->update(timePassed);

			if(m_dontUpdate)
			{
				m_dontUpdate = false;
				return toReturn;
			}

			m_enemies[i].update(timePassed);

			if(m_enemies[i].NextAction() != "")
			{
				if(m_enemies[i].NextAction().find("attacks") != -1 || m_enemies[i].NextAction().find(m_enemies[i].getSpecName()) != -1 )
				{
					if(m_enemies[i].TargetName() == "")
					{
						//Select a target... randomly, to test this for now
					    if(m_players.size() > 0)
					    {
                           short targetIndex = rand() % m_players.size();
					       string target = m_players[targetIndex];
					       m_enemies[i].TargetName() = target;
						   //Attack!
						   //itr->attack(Play ... oh dear, looks like this class also needs a reference to PlayerDatabase =|
						   //enemyFeedback += target;
						   m_enemies[i].NextAction() += " " + target;
						   m_enemies[i].TargetName() = target;
						   m_enemies[i].IsAttackReady() = true;
					    }
						else
						{
						   //enemyFeedback += "...but can't. " + itr->getName() + " yawns.";
						   m_enemies[i].IsAttackReady() = false;
						   //enemyFeedback = "";
						   m_enemies[i].NextAction() = "";
						   m_enemies[i].TargetName() = "";
						   continue;
						}
					}
					else
					{
						//enemyFeedback += /*itr->*/m_enemies[i].TargetName();
						m_enemies[i].IsAttackReady() = true;
						m_enemies[i].NextAction() += m_enemies[i].TargetName();
					}
				}

				toReturn.push_back(&m_enemies[i]);
			}
		}

		return toReturn;
	}

	void Room::addPlayer(string name)
	{
		m_players.push_back(name);
	}

	void Room::removePlayer(string name)
	{
		vector<string>::iterator itr = m_players.begin();
		for(itr = m_players.begin(); itr != m_players.end(); ++itr)
		{
			if( *itr == name)
			{
				m_players.erase(itr);

				//*Also!* --> as player moves, remove him/her as a target from an enemy?
		        vector<Enemy>::iterator enemyItr = m_enemies.begin();
		        for(enemyItr = m_enemies.begin(); enemyItr != m_enemies.end(); ++enemyItr)
		        {
			       if( enemyItr->TargetName() == name)
			       {
  			           enemyItr->TargetName() = "";
			       }
		        }

				return;
			}
		}
	}

	//Search for a string to return from an array of strings... what could possibly go wrong?
	bool Room::checkForPlayer(string name)
	{
		vector<string>::iterator itr = m_players.begin();

		for(itr = m_players.begin(); itr != m_players.end(); ++itr)
		{
		   if(*itr == name)
		   {
		      return true;
		   }
		}

		return false;
	}

	Enemy* Room::findEnemy(string name)
	{
		vector<Enemy>::iterator enemyItr = m_enemies.begin();
		for(enemyItr = m_enemies.begin(); enemyItr != m_enemies.end(); ++enemyItr)
		{
		   if( enemyItr->getName() == name)
		   {
  		      return &(*enemyItr);
		   }
		}

		return 0;
	}

	vector<Enemy*> Room::getAttackingEnemies()
	{
		vector<Enemy*> toReturn;

		vector<Enemy>::iterator enemyItr = m_enemies.begin();
		for(enemyItr = m_enemies.begin(); enemyItr != m_enemies.end(); ++enemyItr)
		{
			if( enemyItr->IsAttackReady() )
			{
				toReturn.push_back( &(*enemyItr) );
			}
		}

		return toReturn;
	}

	void Room::killEnemy(string name, Item* drop)
	{
		vector<Enemy>::iterator enemyItr = m_enemies.begin();
		for(enemyItr = m_enemies.begin(); enemyItr != m_enemies.end(); ++enemyItr)
		{
			if( enemyItr->getName() == name )
			{
				//I wonder...
				if(m_enemies.size() > 1)
				{
				   if(enemyItr != m_enemies.end())
				   {
				      m_enemies.erase(enemyItr);
					  m_dontUpdate = true;
				      break;
				   }
				}
				else
				{
					m_enemies.erase(enemyItr);
				    break;
				}
			}
		}

		//Drop item on floor unless it is loot (that goes straight to a player, currently)
		if(drop->Name() != "loot")
		{
		   m_items.push_back(drop);
		}
	}

	Item* Room::getItem(string name)
	{
		vector<Item*>::iterator itemItr = m_items.begin();
		for(itemItr = m_items.begin(); itemItr != m_items.end(); ++itemItr)
		{
	       if( (*itemItr)->Name() == name)
		   {
		      Item* toReturn = *itemItr;
			  m_items.erase(itemItr);
			  return toReturn; //Errr... hopefully this will work ={
		   }
		}

		return 0;
	}

	vector<string> Room::getPlayers()
	{
		return m_players;
	}

	string Room::listItems()
	{
	   string toReturn;
	   if(m_roomType == "shop")
	   {
		   toReturn = " === ITEMS FOR SALE === \r\n";
	   }
	   vector<Item*>::iterator itr = m_items.begin();
	   for(itr = m_items.begin(); itr != m_items.end(); ++itr)
	   {
		  if(toReturn != "" && m_roomType != "shop")
		  {
			  toReturn += ", ";
		  }

		  toReturn += (*itr)->Name();

		  if(m_roomType == "shop")
		  {
			  toReturn += "\t\t ( " + BasicLib::tostring((*itr)->getSellValue()) + " loot) \r\n";
		  }
	   }

	   return toReturn;
	}

	string Room::listEnemies()
	{
		string toReturn;

	   vector<Enemy>::iterator itr = m_enemies.begin();
	   for(itr = m_enemies.begin(); itr != m_enemies.end(); ++itr)
	   {
		  if(toReturn != "")
		  {
			  toReturn += ", ";
		  }

		  toReturn += itr->getName();
	   }

	   return toReturn;
	}

	//Return info when trying to buy an item that the Parser will deal with
	string Room::attemptPurchase(string itemName)
	{
		vector<Item*>::iterator itemItr = m_items.begin();
		for(itemItr = m_items.begin(); itemItr != m_items.end(); ++itemItr)
		{
	       if( (*itemItr)->Name() == itemName)
		   {
			   return "Would you like to buy the " + itemName + ", for " + BasicLib::tostring( (*itemItr)->getSellValue() ) + " loot?";
		   }
		}

		return "";
	}

	string Room::attemptSale(Item* item, short charisma, bool confirmed)
	{
		//Remember: shopkeepers are interested in profits, so sellValue != purchaseValue...
		//But characters can influence their prices...
		float charismaFactor = 10 / ( (float)( (charisma+1)/2 ) );
		short buyingPrice = item->getSellValue() / (charismaFactor);

		if(confirmed)
		{
			m_items.push_back(item);
			return "You sell your " + item->Name() + " for " + BasicLib::tostring(buyingPrice) + " loot";
		}
		else
		{
		   return "The shopkeeper offers you " + BasicLib::tostring( buyingPrice ) + " for your " + item->Name() + ". Will you sell it?";
		}
	}

	void Room::update(largeNum sinceLastUpdate)
	{
	   m_timePassed += sinceLastUpdate;

	   if(m_timePassed > 10000)
	   {
		   m_timePassed = 0;

		   if(m_roomType == "shop")
		   {
		      //Restock
			   if( m_items.size() < 10)
			   {
				   m_items.push_back( DataStore::getRandomItem(false) );
			   }
		   }
	   }
	}
}