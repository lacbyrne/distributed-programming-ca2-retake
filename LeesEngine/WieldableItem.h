/**

'WieldableItem' -- models attributes of an item that can be wielded in a hand of a player.
In practice, weapons and shields will be instances of this

**/

#ifndef WIELDABLE_ITEM_H
#define WIELDABLE_ITEM_H

#include "Item.h"

namespace LeesEngine
{
   class WieldableItem : public Item
   {
      public:
		  WieldableItem(string p_name, ItemType p_type, string p_description, int p_sellValue, string p_use,
			            short p_minPower, short p_maxPower, short p_accuracy, short p_swingTime, bool p_twoHanded,
						short p_DV, short p_PV, bool p_equipped);
		  short getPower();
		  short getAccuracy() {return m_accuracy;}
		  short getSwingTime();
		  bool isTwoHanded() { return m_twoHanded;}
		  short getPV() { return m_PV;}
		  short getDV() { return m_DV;}

		  bool& isEquipped() {return m_equipped;}

      private:
	      short m_minPower;
		  short m_maxPower;
		  short m_accuracy;
		  short m_swingTime; //Note this is now a multiplier, not an absolute value
		  bool m_twoHanded;
		  short m_PV;
		  short m_DV;
		  bool m_equipped;
   };
}

#endif