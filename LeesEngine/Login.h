#include <string>

#include "../SocketLib/SocketLib.h"
#include "Player.h"
#include "GameData.h"
#include "LoggedIn.h"
//#include "Logs.h"
#include "../SimpleMUD/PlayerDatabase.h"

using std::string;
using namespace SocketLib;

namespace LeesEngine
{
    #ifndef LOGIN_H
    #define LOGIN_H

    class Login : public Telnet::handler
	{
		typedef Telnet::handler thandler;

	    public:
		   Login( Connection<Telnet>& p_conn ); //All classes derived from ConnectionHandler must take in a connection
		   void Handle(string p_data);
		   void Enter();
		   void Leave();
		   void Hungup();
		   void Flooded();
		   static void NoRoom( Connection<Telnet>& p_connection );

	     private:
            string m_name; //For logging in player with
			string m_password; //Player's password
	};

    #endif
}