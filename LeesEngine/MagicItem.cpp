#include "MagicItem.h"

namespace LeesEngine
{
	MagicItem::MagicItem(string p_name, ItemType p_type, string p_description, int p_sellValue, string p_use,
			             /*string p_effect*/short p_effect, short p_charges, short p_range, int p_rechargeTime)
	: Item(p_name, p_type, p_description, p_sellValue, p_use)
	{
		m_effect = p_effect;
		m_charges = p_charges;
		m_range = p_range;
		m_rechargeTime = p_rechargeTime;
	}
}