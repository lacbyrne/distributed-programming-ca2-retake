#include "Actor.h"

#include <iostream>
using std::cout;

namespace LeesEngine
{
   Actor::Actor()
   {
	   //m_name = "";
	   m_maxHP = 0;
	   m_currHP = 0;
	   //m_attributes = {0, 0, 0, 0, 0, 0, 0};
	   m_roomIndex = -1;
	   m_x = 0;
	   m_y = 0;

	   srand( time(NULL) );
   }

   Actor::Actor(string p_name, short p_maxHP, short p_currHP, short p_roomIndex, short p_x, short p_y)
   {
	   //m_name = p_name;
	   m_maxHP = p_maxHP;
	   m_currHP = p_currHP;

	   //Don't forget to actually use the stuff passed in from a child!!
	   m_roomIndex = p_roomIndex;
	   m_x = p_x;
	   m_y = p_y;
   }

   string Actor::attack(Actor& target)
   {
	   string feedback;

	   //feedback += getName() + " attacks " + target.getName() + "!";

	   //Hit chance
	   short hitChance = getAttackAccuracy() - target.getEvasion();
	   //cout << " Attacker accuracy: " << getAttackAccuracy() << " vs target evasion: " << target.getEvasion() << endl;

	   if(hitChance < 1)
	   {
		   hitChance = 1;
	   }

	   //Compare hit chance against a 'twenty sided die'
	   short hitRoll = rand() % 20;

	   //cout << "Hit chance: " << hitChance << " vs Hit roll: " << hitRoll;

	   if(hitChance < hitRoll) //Higher if HIT, LOWER if MISS! Gawd, so confusing...
	   {
		   feedback += "..." + target.getName() + " dodged.";
		   //cout << "dodge" << endl;
		   return feedback;
	   }
       
	   //Damage calculation
	   short damageDealt = getAttackPower() - target.getDefence();
	   //cout << "attack power: " << getAttackPower() << " vs defence: " << target.getDefence() << endl;
	   //cout << "damage: " << damageDealt;

	   /*if(typeid(target) == typeid(Player)
       {
          Player p = (Player)target;
	      damageDealt -= p->getBlockChance(getAttackTime());

	      if(damageDealt == 0)
	      {
             return p->name + " managed to completely block the blow..."
	      }
	   }*/
	   if(damageDealt < 1)
	   {
		  damageDealt = 1;
	   }
	   
	   feedback += handleDamagePhase(target, damageDealt);

	   return feedback;
   }

   string Actor::specialAttack(Actor& target, short specAcc, short specPower)
   {
	   string feedback;

	   short hitChance = specAcc - ( (target.getEvasion() /*+ target.getSpirit()*/ ) );

	   if(hitChance < 1)
	   {
		   hitChance = 1;
	   }

	   //Compare hit chance against a 'twenty sided die'
	   short hitRoll = rand() % 20;

	   if(hitChance < hitRoll) //Higher if HIT, LOWER if MISS! Gawd, so confusing...
	   {
		   feedback += "..." + target.getName() + " dodged.";
		   return feedback;
	   }	
       
	   //Damage calculation
	   short damageDealt = specPower - target.getSpirit();
	   //cout << "attack power: " << getAttackPower() << " vs defence: " << target.getDefence() << endl;
	   //cout << "damage: " << damageDealt;

	   /*if(typeid(target) == typeid(Player)
       {
          Player p = (Player)target;
	      damageDealt -= p->getBlockChance(getAttackTime());

	      if(damageDealt == 0)
	      {
             return p->name + " managed to completely block the blow..."
	      }
	   }*/
	   if(damageDealt < 1)
	   {
		  damageDealt = 1;
	   }
	   
	   feedback += handleDamagePhase(target, damageDealt);

	   return feedback;
   }

   string Actor::handleDamagePhase(Actor& target, short damage)
   {
	  string feedback;

      target.CurrHP() -= damage;
	  feedback = target.getName() + " took " + BasicLib::tostring(damage) + " damage!";

	   short yields = target.checkKO();
	   if(yields != 0)
	   {
		   //feedback += "\r\n" + target.getName() + " is slain!!"; //Already taken care of?
		   //How to identify if victor is player or enemy??

		   winBattle(yields, target.isEnemy() );
	   }

	   return feedback;
   }

   string Actor::getName()
   {
	   /*try
	   {
		   return m_name;
	   }
	   catch(...)
	   {
		   cout << "problem...";
		   return "problem";
	   }*/
	   return "error";
   }
}