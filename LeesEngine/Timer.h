/**

'Timer' -- a customised solution to tracking game time as a substitute for Ron Penton's Basic Library

**/

#ifndef TIMER_H
#define TIMER_H

#define _WINSOCKAPI_ //**Need to include this since the include chain make cause Windows to try to include Winsock twice... or something like that
#include <Windows.h>

#include "Typedefs.h"

#include <iostream>

using std::cout;
using std::endl;

namespace LeesEngine
{
   class Timer
   {
      public:
		 Timer(int dummy);
		 largeNum getTimeElapsed();
		 bool hasTimePassed(short p_time);
		 largeNum getSinceLastUpdate();

      private:
		 largeNum initSysTime;
		 largeNum counter; //How much time has actually passed so far (since running application)
		 largeNum totalDist;
		 largeNum totalRate;
		 largeNum subCounter;
		 largeNum oldCounter;
		 largeNum sinceLastUpdate;
   };
}

#endif