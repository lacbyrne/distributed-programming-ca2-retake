<ROOM INDEX>3
<ROOM NAME>Fourth room
<DANGER LEVEL>3
<EXITS>-1,-1,-1,1
<DESCRIPTION>Fourth room. It appears to be a cul-de-sac. You fear that quite dangerous foes might pop up here.
<ITEMS>
<ENEMIES>minotaur 41, crazed barbarian 40, minotaur 50, golem 60
<TYPE>regular