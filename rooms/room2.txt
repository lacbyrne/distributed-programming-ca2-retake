<ROOM INDEX>1
<ROOM NAME>Second room
<DANGER LEVEL>1
<EXITS>2,0,3,4
<DESCRIPTION>Second room. It's very spacious.
<ITEMS>leather armour, useless item, leather armour, bronze plate mail, behemoth suit, spear
<ENEMIES>skeleton 5, skeleton 5, goblin 8
<TYPE>regular